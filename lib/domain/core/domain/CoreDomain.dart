import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:sol_app/domain/core/entity/slot/SlotsStore.dart';
import 'package:sol_app/domain/core/entity/space/SpacesStore.dart';
import 'package:sol_app/domain/core/entity/task/TaskCreateState.dart';
import 'package:sol_app/domain/core/entity/task/TaskCreateViewModel.dart';
import 'package:sol_app/domain/core/entity/task/TasksStore.dart';
import 'package:sol_app/domain/core/entity/view/ViewStore.dart';
import 'package:sol_app/domain/user/domain/UserDomain.dart';
import 'package:sol_app/domain/user/entity/UserState.dart';
import 'package:sol_app/infrastructure/api/task/response/TaskResponse.dart';
import 'package:sol_app/infrastructure/api/task/service/fetchCreateTask.dart';

class CoreDomain extends ChangeNotifier {
  final SpacesStore spacesStore = SpacesStore();
  final TasksStore tasksStore = TasksStore();
  final TaskCreateViewModel createForm = TaskCreateViewModel();
  final SlotStore slotStore = SlotStore();
  final ViewStore viewStore = ViewStore();
  final UserDomain userDomain;

  CoreDomain(this.userDomain);

  void setup() {
    userDomain.addListener(userDidChange);
    createForm.notifier = this;
    spacesStore.notifier = this;
    tasksStore.notifier = this;
    slotStore.notifier = this;
    createForm.cleanUpRequest();
  }

  void userDidChange() {
    loadSpaces();
  }

  void loadSpaces() async {
    if (userDomain.userEntity.state != UserState.logged) return;
    await spacesStore.loadSpaces();
    await tasksStore.loadTasks();
    await viewStore.load();
  }

  void createSlot() async {

  }

  void createTask(bool createNext) async {
    if (createForm.request.title == null || createForm.request.title == '') {
      return;
    }

    HapticFeedback.heavyImpact();
    createForm.inProgress = true;
    notifyListeners();

    try {
      TaskResponse taskResult = await fetchCreateTask(createForm.request);
      slotStore.commitDrafts(taskResult.result!);

      if (taskResult != null && taskResult.result != null) {
        createForm.cleanUpRequest();
        tasksStore.updateOrCreateTask(taskResult.result!);
        if (createNext == false) {
          createForm.state = TaskCreateState.none;
        }
        return;
      }
    } catch (e) {
      print(e);
    }

    createForm.inProgress = false;
    notifyListeners();
  }
}
