import 'package:flutter/cupertino.dart';
import 'package:sol_app/domain/core/entity/task/TaskEntity.dart';
import 'package:sol_app/infrastructure/api/task/request/TaskDoneRequest.dart';
import 'package:sol_app/infrastructure/api/task/response/TaskResponse.dart';
import 'package:sol_app/infrastructure/api/task/response/TaskResult.dart';
import 'package:sol_app/infrastructure/api/task/response/TasksResponse.dart';
import 'package:sol_app/infrastructure/api/task/service/fetchTaskDone.dart';
import 'package:sol_app/infrastructure/api/task/service/fetchTasks.dart';

class TasksStore {
  List<String> tasks = [];
  Map<String, TaskEntity> byTaskId = <String, TaskEntity>{};
  Map<String, List<String>> bySpaceId = <String, List<String>>{};
  ChangeNotifier? notifier;

  void makeDoneOrOpen(String taskId) async {
    if (byTaskId.containsKey(taskId) == true) {
      if (byTaskId[taskId]?.taskResult.status == TaskStatus.DONE) {
        TaskResponse task = await fetchTaskDone(TaskDoneRequest(taskId));
        byTaskId[taskId]?.taskResult.status = TaskStatus.OPEN;
      } else {
        TaskResponse task = await fetchTaskDone(TaskDoneRequest(taskId));
        byTaskId[taskId]?.taskResult.status = TaskStatus.DONE;
      }

      notifier?.notifyListeners();
    }
  }

  List<String> getTaskListBySpaceId(String spaceId) {
    if (bySpaceId[spaceId] == null) {
      bySpaceId[spaceId] = [];
    }

    return bySpaceId[spaceId]!;
  }

  void processTask(TaskResult task) {
    TaskEntity taskEntity = TaskEntity(task);
    String taskId = taskEntity.taskResult.id!;
    String spaceId = taskEntity.taskResult.spaceId!;

    byTaskId[taskId] = taskEntity;

    if (bySpaceId.containsKey(spaceId) == false) {
      bySpaceId[spaceId] = [];
    }

    for (var i = 0; i < bySpaceId[spaceId]!.length; i++) {
      if (bySpaceId[spaceId]!.contains(taskId) == false) {
        bySpaceId[spaceId]!.add(taskId);
      }
    }

    if (tasks.contains(taskId) == false) {
      tasks.add(taskId);
    }
  }

  void updateOrCreateTask(TaskResult task) {
    processTask(task);
    notifier?.notifyListeners();
  }

  Future<bool> loadTasks() async {
    try {
      TasksResponse tasksResponse = await fetchTasks();
      if (tasksResponse.result != null) {
        List<String> resultList = [];
        Map<String, TaskEntity> resultByTask = <String, TaskEntity>{};
        Map<String, List<String>> resultBySpace = <String, List<String>>{};

        for (var task in tasksResponse.result!) {
          TaskEntity taskEntity = TaskEntity(task);
          String taskId = taskEntity.taskResult.id!;
          String spaceId = taskEntity.taskResult.spaceId!;

          resultList.add(taskId);
          resultByTask[task.id!] = taskEntity;

          if (resultBySpace.containsKey(spaceId) == false) {
            resultBySpace[spaceId] = [];
          }

          if (resultBySpace[spaceId]!.contains(taskId) == false) {
            if (taskEntity.taskResult.parentTaskId == null) {
              resultBySpace[spaceId]!.add(taskId);
            }
          }
        }

        for (var spaceId in resultBySpace.keys) {
          // TODO тут сортировка
          // resultBySpace[spaceId]!.sort((String taskA, String taskB){
          //   return byTaskId[taskA].taskResult.;
          // });
        }

        tasks = resultList;
        byTaskId = resultByTask;
        bySpaceId = resultBySpace;
        return true;
      }
    } catch (e) {}
    notifier?.notifyListeners();
    return false;
  }
}
