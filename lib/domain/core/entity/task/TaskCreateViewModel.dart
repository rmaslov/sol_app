import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:sol_app/domain/core/entity/task/TaskCreateState.dart';
import 'package:sol_app/infrastructure/api/space/response/SpacesResponse.dart';
import 'package:sol_app/infrastructure/api/task/request/CreateTaskRequest.dart';
import 'package:sol_app/infrastructure/api/view/response/ViewResult.dart';

class TaskCreateViewModel {
  TaskCreateState state = TaskCreateState.none;
  bool showViewsList = false;
  bool showEmojiKeyboard = false;
  bool showSpacesList = false;
  bool showDeadlines = false;
  CreateTaskRequest request = CreateTaskRequest();
  ChangeNotifier? notifier;
  bool inProgress = false;
  String subtitle = '';
  String spaceTitle = '';
  List<ViewResult> views = [];

  DateTime fastDateToday() {
    DateTime time = DateTime.now();
    time = DateTime(time.year, time.month, time.day, 23, 59, 59, 0, 0);
    return time;
  }

  DateTime fastDateTomorrow() {
    DateTime time = DateTime.now();
    time = DateTime(time.year, time.month, time.day, 23, 59, 59, 0, 0);
    time = time.add(Duration(days: 1));
    return time;
  }

  DateTime fastDateThisWeek() {
    DateTime time = DateTime.now();
    time = DateTime(time.year, time.month, time.day, 23, 59, 59, 0, 0);
    time = time.add(Duration(days: DateTime.daysPerWeek - time.weekday));
    return time;
  }

  DateTime fastDateNextWeek() {
    DateTime time = DateTime.now();
    time = DateTime(time.year, time.month, time.day, 23, 59, 59, 0, 0);
    time = time.add(Duration(days: DateTime.daysPerWeek - time.weekday));
    time = time.add(Duration(days: 1));
    time = time.add(Duration(days: DateTime.daysPerWeek - time.weekday));
    return time;
  }

  void updateSubtitle() {
    subtitle = spaceTitle;
    if (request.deadline != null) {
      DateTime dt = DateTime.fromMillisecondsSinceEpoch(request.deadline!);
      subtitle += addSubtitle(subtitle, DateFormat('MMM dd').format(dt));
    }
    if(this.views.isEmpty == false){
      for(var e in views){
        subtitle += addSubtitle(subtitle, e.view!.fullTitle());
      }
    }
  }

  String addSubtitle(String subtitle, String delta) {
    String result = '';
    if (subtitle != '') result = result + ' · ';
    result = result + delta;
    return result;
  }

  void cleanUpRequest() {
    request = CreateTaskRequest();
    request.icon = null;
    request.planningPoints = [];
    request.title = '';
    request.viewIds = [];
    request.reminders = [];
    inProgress = false;
  }

  void setDeadline(DateTime? dateTime) {
    request.deadline = (dateTime?.millisecondsSinceEpoch);
    updateSubtitle();
    notifier?.notifyListeners();
  }

  void setTitleSilent(String title, bool silent) {
    request.title = title;
    if (silent == false) {
      notifier?.notifyListeners();
    }
  }

  void showForm() {
    state = TaskCreateState.createForm;
    notifier?.notifyListeners();
  }

  void hideForm() {
    state = TaskCreateState.none;
    notifier?.notifyListeners();
  }

  void toggleEmoji() {
    showEmojiKeyboard = !showEmojiKeyboard;
    showDeadlines = false;
    showSpacesList = false;
    showViewsList = false;
    notifier?.notifyListeners();
  }

  void setSpace(String id, String title) {
    request.spaceId = id;
    spaceTitle = title;
    updateSubtitle();
  }

  void toggleView(ViewResult view) {
    if (request.viewIds == null) request.viewIds = [];

    if (request.viewIds?.contains(view.id) == false) {
      addView(view);
    } else {
      removeView(view);
    }

    updateSubtitle();
    notifier?.notifyListeners();
  }

  void addView(ViewResult view) {
    if (request.viewIds == null) request.viewIds = [];

    if (request.viewIds?.contains(view.id) == false) {
      request.viewIds?.add(view.id!);
      views.add(view);
    }
  }

  void removeView(ViewResult view) {
    if (request.viewIds == null) request.viewIds = [];

    if (request.viewIds?.contains(view.id) == true) {
      request.viewIds?.removeWhere((element) => element == view.id);
      views.removeWhere((element) => (element.id == view.id));
    }
  }

  void setEmoji(String icon) {
    IconResult iconResult = IconResult(data: icon, type: 'EMOJI');
    request.icon = iconResult;
    request.icon?.data = icon;
    request.icon?.type = 'EMOJI';

    showEmojiKeyboard = false;
    notifier?.notifyListeners();
  }

  void closeCalendar() {
    state = TaskCreateState.createForm;
    notifier?.notifyListeners();
  }

  void toggleViews() {
    showViewsList = !showViewsList;
    showSpacesList = false;
    showEmojiKeyboard = false;
    showDeadlines = false;
    notifier?.notifyListeners();
  }

  void toggleSpaces() {
    showSpacesList = !showSpacesList;
    showEmojiKeyboard = false;
    showViewsList = false;
    showDeadlines = false;
    notifier?.notifyListeners();
  }

  void hideAll() {
    showSpacesList = false;
    showDeadlines = false;
    showViewsList = false;
    showEmojiKeyboard = false;
    notifier?.notifyListeners();
  }

  void toggleDeadlines() {
    showSpacesList = false;
    showEmojiKeyboard = false;
    showViewsList = false;
    showDeadlines = !showDeadlines;
    notifier?.notifyListeners();
  }

  void addNotification(DateTime dateTime){
    if(request.reminders == []) request.reminders = [];
    request.reminders!.add(dateTime.millisecondsSinceEpoch);
    notifier?.notifyListeners();
  }

  void removeNotification(DateTime dateTime){
    if(request.reminders == []) request.reminders = [];
    request.reminders!.removeWhere((element) => dateTime.millisecondsSinceEpoch == element);
    notifier?.notifyListeners();
  }
}
