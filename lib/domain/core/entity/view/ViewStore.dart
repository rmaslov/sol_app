import 'package:flutter/cupertino.dart';
import 'package:sol_app/domain/core/entity/view/ViewEntity.dart';
import 'package:sol_app/infrastructure/api/view/response/ViewsResponse.dart';
import 'package:sol_app/infrastructure/api/view/service/fetchViews.dart';

class ViewStore {
  List<ViewEntity> views = [];
  ChangeNotifier? notifier;

  Future<bool> load() async {
    try {
      ViewsResponse response = await fetchViews();
      if (response.result != null) {
        views = response.result!.map((e) => ViewEntity(e)).toList();
        notifier?.notifyListeners();
        return true;
      }
    } catch (e) {}
    notifier?.notifyListeners();
    return false;
  }
}
