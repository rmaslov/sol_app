import 'dart:ui';

import 'package:intl/intl.dart';
import 'package:sol_app/domain/core/entity/meeting/MeetingType.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotResult.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

/// Custom business object class which contains properties to hold the detailed
/// information about the event data which will be rendered in calendar.
class Meeting {
  /// Creates a meeting class with required details.
  Meeting(this.id, this.type, this.eventName, this.from, this.to,
      this.background, this.isAllDay);

  String id;
  MeetingType type;

  /// Event name which is equivalent to subject property of [Appointment].
  String eventName;

  /// From which is equivalent to start time property of [Appointment].
  DateTime from;

  /// To which is equivalent to end time property of [Appointment].
  DateTime to;

  /// Background which is equivalent to color property of [Appointment].
  Color background;

  /// IsAllDay which is equivalent to isAllDay property of [Appointment].
  bool isAllDay;

  String prettyDate() {
    if (from.year == to.year && from.month == to.month && from.day == to.day) {
      return '${DateFormat('EEEE, MMM dd yyyy,').format(from)} \n'
          'from ${DateFormat('hh:mm a').format(from)} to ${DateFormat('hh:mm a').format(to)}';
    } else {
      return 'from ${DateFormat('hh:mm a EE, MMM dd yyyy').format(from)} \n'
          'to ${DateFormat('hh:mm a EE, MMM dd yyyy').format(to)}';
    }
  }

  String external(){
    return type == MeetingType.draft ? 'Draft event':(
        type == MeetingType.slot ? 'Event from task':'Calendar event'
    );
  }

  static Meeting fromSlot(SlotResult slot){
    Meeting meeting = Meeting(
        slot.id!,
        MeetingType.slot, slot.title!,
        DateTime.fromMillisecondsSinceEpoch(slot.startTime!),
        DateTime.fromMillisecondsSinceEpoch(slot.endTime!),
        SolColors.current.meeting.slot,
        false);
    return meeting;
  }
}
