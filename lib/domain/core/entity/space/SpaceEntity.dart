import 'package:sol_app/domain/core/entity/space/SpaceView.dart';
import 'package:sol_app/infrastructure/api/space/response/SpacesResponse.dart';

class SpaceEntity{
  SpaceResult spaceResult;
  SpaceView spaceView = SpaceView();

  SpaceEntity(this.spaceResult);
}