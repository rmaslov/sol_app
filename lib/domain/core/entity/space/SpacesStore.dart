import 'package:flutter/cupertino.dart';
import 'package:sol_app/domain/core/entity/space/SpaceEntity.dart';
import 'package:sol_app/infrastructure/api/space/response/SpacesResponse.dart';
import 'package:sol_app/infrastructure/api/space/service/fetchSpaces.dart';

class SpacesStore {
  List<SpaceEntity> spaces = [];
  ChangeNotifier? notifier;

  Future<void> loadSpaces() async {
    try {
      SpacesResponse spacesResponse = await fetchSpaces();
      if (spacesResponse.result != null) {
        spaces = spacesResponse.result!.map((e) => SpaceEntity(e)).toList();
      } else {
        spaces = [];
      }
    } catch (e) {
      spaces = [];
    }

    notifier?.notifyListeners();
  }

  bool toggleSpace(String id) {
    for (var element in spaces) {
      if (element.spaceResult.id == id) {
        element.spaceView.toggle();
        notifier?.notifyListeners();
        return true;
      }
    }
    return false;
  }

  void toggleAllSpaces(bool show) {
    for (var element in spaces) {
      element.spaceView.toggleShow(show);
    }
    notifier?.notifyListeners();
  }
}
