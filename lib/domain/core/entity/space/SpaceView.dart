class SpaceView {
  bool showTasksOnGeneralScreen = false;

  void toggle(){
    showTasksOnGeneralScreen = !showTasksOnGeneralScreen;
  }

  void toggleShow(bool show){
    showTasksOnGeneralScreen = show;
  }
}
