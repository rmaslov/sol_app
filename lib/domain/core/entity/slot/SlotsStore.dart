import 'package:flutter/cupertino.dart';
import 'package:sol_app/domain/core/entity/meeting/Meeting.dart';
import 'package:sol_app/domain/core/entity/meeting/MeetingType.dart';
import 'package:sol_app/domain/core/entity/slot/SlotEntity.dart';
import 'package:sol_app/infrastructure/api/slot/request/SlotRequest.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotResponse.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotResult.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotsResponse.dart';
import 'package:sol_app/infrastructure/api/slot/service/fetchCreateSlot.dart';
import 'package:sol_app/infrastructure/api/slot/service/fetchDeleteSlot.dart';
import 'package:sol_app/infrastructure/api/slot/service/fetchSlots.dart';
import 'package:sol_app/infrastructure/api/task/response/TaskResult.dart';
import 'package:sol_app/ui/styles/SolColors.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:uuid/uuid.dart';

class SlotStore {
  List<SlotEntity> slots = [];
  List<Meeting> meetings = [];
  List<Meeting> drafts = [];
  Set<String> slotIds = {};
  CalendarView calendarView = CalendarView.day;
  ChangeNotifier? notifier;

  void cleanDraft() {
    drafts = [];
    update();
  }

  void commitDrafts(TaskResult taskResult) async {
    for (var meeting in drafts) {
      SlotRequest request = SlotRequest();
      request.taskId = taskResult.id;
      request.startTime = meeting.from.millisecondsSinceEpoch;
      request.endTime = meeting.to.millisecondsSinceEpoch;
      request.timezone = meeting.from.timeZoneOffset.inHours;
      SlotResponse response = await fetchCreateSlot(request);
      add(response.result!);
    }
    cleanDraft();
    update();
  }

  void updateMeetingDrafts(String title) {
    for (var meeting in meetings) {
      meeting.eventName = title;
    }
  }

  void changeCalendarType(CalendarView calendarView) {
    this.calendarView = calendarView;
    notifier?.notifyListeners();
  }

  void setUp(ChangeNotifier? notifier) {
    this.notifier = notifier;
  }

  Meeting? getMeeting(String id) {
    for (var meeting in meetings) {
      if (meeting.id == id) {
        return meeting;
      }
    }
    return null;
  }

  void update() {
    final List<Meeting> toUpdateList = <Meeting>[];
    toUpdateList.addAll(drafts);

    for (var slot in slots) {
      toUpdateList.add(Meeting.fromSlot(slot.slotResult));
    }

    meetings = toUpdateList;
    notifier?.notifyListeners();
  }

  void addDraft(String eventName, DateTime from, DateTime to, bool isAllDay) {
    Uuid uuid = Uuid();
    Meeting meeting = Meeting(uuid.v4(), MeetingType.draft, eventName, from, to,
        SolColors.current.meeting.color(MeetingType.draft), isAllDay);
    drafts.add(meeting);
    update();
  }

  updateTime(Meeting meetingForUpdate, DateTime start) {
    if (meetingForUpdate.type == MeetingType.draft) {
      for (var meeting in drafts) {
        if (meeting.id == meetingForUpdate.id) {
          var delta = meeting.to.millisecondsSinceEpoch -
              meeting.from.millisecondsSinceEpoch;
          DateTime end = DateTime.fromMillisecondsSinceEpoch(
              start.millisecondsSinceEpoch + delta);
          meeting.from = start;
          meeting.to = end;
        }
      }
    }
    if (meetingForUpdate.type == MeetingType.slot) {
      for (var slot in slots) {
        if (slot.slotResult.id == meetingForUpdate.id) {
          var delta = slot.slotResult.endTime! - slot.slotResult.startTime!;
          DateTime end = DateTime.fromMillisecondsSinceEpoch(
              start.millisecondsSinceEpoch + delta);
        }
      }
    }
    notifier?.notifyListeners();
  }

  void deleteDraft(Meeting meeting) {
    if (meeting.type != MeetingType.draft) return;
    drafts.removeWhere((element) => element.id == meeting.id);
    update();
  }

  void deleteSlot(Meeting meeting) {
    if (meeting.type != MeetingType.slot) return;
    fetchDeleteSlot(meeting.id);
    slots.removeWhere((element) => element.slotResult.id == meeting.id);
    update();
  }

  void add(SlotResult result) {
    if (slotIds.contains(result.id)) {
      for (var i = 0; i < slots.length; i++) {
        if (slots[i].slotResult.id == result.id) {
          slots[i].slotResult = result;
        }
      }
    } else {
      slotIds.add(result.id!);
      slots.add(SlotEntity(result));
    }
    update();
  }

  Future<void> loadSlots(DateTime date) async {
    try {
      for (var i = 0; i < 14; i++) {
        DateTime d = DateTime.now();
        d.add(Duration(days: -7 + i));
        SlotsResponse slotsResponse = await fetchSlotsByDate(d);
        slotsResponse.result?.items?.forEach((element) {
          add(element);
        });
      }
      update();
    } catch (e) {}
  }
}
