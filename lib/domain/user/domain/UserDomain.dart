import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:sol_app/domain/user/entity/UserEntity.dart';
import 'package:sol_app/domain/user/entity/UserState.dart';
import 'package:sol_app/infrastructure/SolPreferences.dart';
import 'package:sol_app/infrastructure/api/baseAuth/request/LoginRequest.dart';
import 'package:sol_app/infrastructure/api/baseAuth/request/RefreshTokenRequest.dart';
import 'package:sol_app/infrastructure/api/baseAuth/response/LoginResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/response/MeResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchLoginEmail.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchMe.dart';

class UserDomain extends ChangeNotifier {
  final UserEntity userEntity = UserEntity();

  void cleanUpLoginForm(){
    userEntity.state = UserState.unlogged;
    userEntity.hasError = false;
    userEntity.userId = null;
    userEntity.username = null;
    userEntity.formIsLoading = false;
    userEntity.error = '';

    notifyListeners();
  }

  void successLoginForm(){
    userEntity.state = UserState.unlogged;
    userEntity.hasError = false;
    userEntity.userId = null;
    userEntity.username = null;
    userEntity.error = '';
    userEntity.formIsLoading = false;
    notifyListeners();
  }

  void loadingLoginForm(){
    userEntity.state = UserState.unlogged;
    userEntity.hasError = false;
    userEntity.userId = null;
    userEntity.username = null;
    userEntity.error = '';
    userEntity.formIsLoading = true;
    notifyListeners();
  }

  void errorLoginForm(String error){
    userEntity.state = UserState.unlogged;
    userEntity.hasError = true;
    userEntity.error = error;
    userEntity.userId = null;
    userEntity.username = null;
    userEntity.formIsLoading = false;
    notifyListeners();
  }

  void loginByEmail(String email, String password) async {
    if (email == '' || password == '') {
      errorLoginForm('Email or password is empty');
      return;
    }

    loadingLoginForm();

    try {
      LoginResponse loginResponse =
          await fetchLoginEmail(LoginRequest(email: email, password: password));
      initUser();
    } catch (e) {
      errorLoginForm('Email or password is incorrect');
    }
  }

  void initUser() async {
    try {
      // final prefs = await SolPreferences.instance.initPreferences();
      var refreshToken = "";//SolPreferences.instance.refreshToken();
      if (refreshToken != null) {
        await fetchRefreshToken(
            RefreshTokenRequest(refreshToken: refreshToken));
      }

      MeResponse? meResponse = await fetchMe();
      //SpacesResponse spacesResponse = await fetchSpaces();

      if (meResponse != null && meResponse.result != null) {
        userEntity.state = UserState.logged;
        userEntity.userId = meResponse.result!.id;
        userEntity.username = meResponse.result!.username;
      } else {
        userEntity.state = UserState.unlogged;
        userEntity.username = null;
        userEntity.userId = null;
      }
    } catch (e) {
      userEntity.state = UserState.unlogged;
    }

    notifyListeners();
  }
}
