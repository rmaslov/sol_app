import 'package:sol_app/domain/user/entity/UserState.dart';

class UserEntity {
  String? userId;
  String? username;
  UserState state = UserState.init;

  //Login by email form
  String  error = '';
  bool hasError = false;
  bool formIsLoading = false;
}

