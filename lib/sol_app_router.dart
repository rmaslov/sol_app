class Routes {
  static String loginByEmail = '/login-by-email';
  static String login = '/login';

  static String general = '/general';
  //static String calendar = '/calendar';

  static String root = '/';
}



// GoRouter _router() {
//   return GoRouter(
//     initialLocation: '/',
//     navigatorKey: rootNavigatorKey,
//     errorBuilder: errorBuilder,
//     routes: [
//       GoRoute(
//           path: '/',
//           parentNavigatorKey: rootNavigatorKey,
//           pageBuilder: rootPage,
//           routes: _buildRoutes())
//     ],
//   );
// }