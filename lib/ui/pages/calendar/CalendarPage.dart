import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/components/calendar/CalendarComponent.dart';
import 'package:sol_app/ui/components/calendar/CalendarOpenFrom.dart';

class CalendarPage extends StatefulWidget {
  const CalendarPage(this.openFrom, {Key? key}) : super(key: key);
  final CalendarOpenFrom openFrom;

  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  @override
  Widget build(BuildContext context) {
    return CalendarComponent(widget.openFrom);
  }
}
