import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/sol_app_router.dart';
import 'package:sol_app/domain/user/domain/UserDomain.dart';
import 'package:sol_app/domain/user/entity/UserState.dart';
import 'package:sol_app/ui/components/buttons/ButtonState.dart';
import 'package:sol_app/ui/components/buttons/SolButton.dart';
import 'package:sol_app/ui/components/buttons/styles/DefaultTextFieldTextStyle.dart';
import 'package:sol_app/ui/components/buttons/styles/ErrorTextFieldTextStyle.dart';
import 'package:sol_app/ui/components/text/LoginHeaderText.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class LoginByEmail extends StatefulWidget {
  const LoginByEmail({Key? key}) : super(key: key);

  @override
  State<LoginByEmail> createState() => _LoginByEmailState();
}

class _LoginByEmailState extends State<LoginByEmail> {
  late TextEditingController emailController;
  late TextEditingController passwordController;

  @override
  void initState() {
    super.initState();
    emailController = TextEditingController(text: '');
    passwordController = TextEditingController(text: '');

    Future.delayed(Duration.zero, () {
      UserDomain userDomain = Provider.of<UserDomain>(context, listen: false);
      userDomain.addListener(userDidChange);
      userDomain.cleanUpLoginForm();
    });
  }

  @override
  void deactivate() {
    super.deactivate();
    UserDomain userDomain = Provider.of<UserDomain>(context, listen: false);
    userDomain.removeListener(userDidChange);
  }

  void userDidChange() {
    UserDomain userDomain = Provider.of<UserDomain>(context, listen: false);
    if (userDomain.userEntity.state == UserState.logged) {
      Navigator.pushNamedAndRemoveUntil(
          context, Routes.general, (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          previousPageTitle: 'Back',
          leading: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CupertinoNavigationBarBackButton(
                previousPageTitle: "Back",
                color: SolColors.current.text.primary,
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        ),
        child: ListView(
          children: <Widget>[
            SafeArea(
              child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: [
                          LoginHeaderText(
                            "Mail so mail,\nenter\nit and password",
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 87,
                      ),
                      Consumer<UserDomain>(
                        builder: (context, user, child) {
                          return Text(
                            (user.userEntity.hasError
                                ? user.userEntity.error
                                : ''),
                            style: TextStyle(
                                fontSize: 16,
                                color: SolColors.current.field.textError),
                          );
                        },
                      ),
                      const SizedBox(height: 16),
                      CupertinoTextField(
                        controller: emailController,
                        placeholder: 'Email',
                        style: DefaultTextFieldTextStyle,
                        padding: EdgeInsets.fromLTRB(8, 14, 8, 14),
                        keyboardType: TextInputType.emailAddress,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: SolColors.current.field.background),
                      ),
                      const SizedBox(height: 16),
                      CupertinoTextField(
                        obscureText: true,
                        controller: passwordController,
                        placeholder: 'Password',
                        style: DefaultTextFieldTextStyle,
                        padding: EdgeInsets.fromLTRB(8, 14, 8, 14),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: SolColors.current.field.background),
                      ),
                      const SizedBox(height: 16),
                      Consumer<UserDomain>(builder: (context, user, child) {
                        return SolButton('Email',
                          state: user.userEntity.formIsLoading == false
                              ? ButtonState.defaultState
                              : ButtonState.loading,
                          onPressed: (() {
                            Provider.of<UserDomain>(context, listen: false)
                                .loginByEmail(emailController.value.text,
                                    passwordController.value.text);
                          }),
                        );
                      }),
                      const SizedBox(height: 16),
                    ],
                  )),
            ),
          ],
        ));
  }
}
