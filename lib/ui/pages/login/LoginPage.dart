import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/components/buttons/AppleIdButton.dart';
import 'package:sol_app/ui/components/buttons/EmailButton.dart';
import 'package:sol_app/ui/components/buttons/FacebookButton.dart';
import 'package:sol_app/ui/components/buttons/GoogleButton.dart';
import 'package:sol_app/ui/components/text/LoginHeaderText.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: SafeArea(
        child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                const SizedBox(
                  height: 24,
                ),
                Row(
                  children: [
                    Flexible(
                        child: LoginHeaderText(
                            "Hey!\nUnload your head and do it.\n\nLet's log in first")),
                  ],
                ),
                const Spacer(),
                const AppleIdButton(),
                const SizedBox(height: 16),
                const FacebookButton(),
                const SizedBox(height: 16),
                const GoogleButton(),
                const SizedBox(height: 16),
                const EmailButton(),
                const SizedBox(height: 44)
              ],
            )),
      ),
    );
  }
}
