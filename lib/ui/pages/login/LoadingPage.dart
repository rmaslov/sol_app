import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/sol_app_router.dart';
import 'package:sol_app/domain/user/domain/UserDomain.dart';
import 'package:sol_app/domain/user/entity/UserState.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class LoadingPage extends StatefulWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      UserDomain userDomain = Provider.of<UserDomain>(context, listen: false);
      userDomain.initUser();
      userDomain.addListener(userDidChange);
    });
  }

  @override
  void deactivate() {
    super.deactivate();
    UserDomain userDomain = Provider.of<UserDomain>(context, listen: false);
    userDomain.removeListener(userDidChange);
  }

  void userDidChange() {
    UserDomain userDomain = Provider.of<UserDomain>(context, listen: false);
    if (userDomain.userEntity.state == UserState.logged) {
      Navigator.pushNamedAndRemoveUntil(
          context, Routes.general, (route) => false);
    }
    if (userDomain.userEntity.state == UserState.unlogged) {
      Navigator.pushNamedAndRemoveUntil(
          context, Routes.login, (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserDomain>(
      builder: (context, cart, child) => CupertinoPageScaffold(
          child: Center(
        child: CircularProgressIndicator(
          color: SolColors.current.button.primary,
        ),
      )),
    );
  }
}
