import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/spaceItem/SpaceItem.dart';
import 'package:sol_app/ui/components/spaceItem/SpaceTaskItem.dart';
import 'package:sol_app/ui/container/SolContainer.dart';
import 'package:sol_app/ui/components/general/GeneralHeader.dart';
import 'package:sol_app/ui/components/general/SpaceHeader.dart';
import 'package:sol_app/ui/components/stories/Stories.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class GeneralPage extends StatefulWidget {
  const GeneralPage({Key? key}) : super(key: key);

  @override
  State<GeneralPage> createState() => _GeneralPageState();
}

class TestData{
  int test = 1;
  String bla = "sdf";
}

class _GeneralPageState extends State<GeneralPage> {
  TestData _testData = TestData();


  @override
  Widget build(BuildContext context) {
    return SolContainer(
      CupertinoPageScaffold(
          navigationBar: null,
          child: ListView(
            children: <Widget>[
              Column(
                children: [
                  const SizedBox(
                    height: 24,
                  ),
                  const GeneralHeader(),
                  const Stories(),
                  const SizedBox(
                    height: 24,
                  ),
                  const SpaceHeader(),
                  Consumer<CoreDomain>(builder: (context, domain, child) {
                    List<Widget> spaces = [];
                    for (var store in domain.spacesStore.spaces) {
                      spaces.add(SpaceItem(
                        store,
                        onClickToggle: () {
                          domain.spacesStore.toggleSpace(store.spaceResult.id!);
                        },
                      ));
                      if (store.spaceView.showTasksOnGeneralScreen == true) {
                        for(String taskId in domain.tasksStore.getTaskListBySpaceId(store.spaceResult.id!)){
                          spaces.add(SpaceTaskItem(domain.tasksStore.byTaskId[taskId]!));
                        }
                        spaces.add(Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 4),
                            child: Container(
                              height: 12,
                              decoration: BoxDecoration(
                                  color: SolColors
                                      .current.spaceItem.backgroundToggleOpen,
                                  borderRadius: const BorderRadius.only(
                                    bottomLeft: Radius.circular(12.0),
                                    bottomRight: Radius.circular(12.0),
                                  )),
                              child: Row(),
                            )));
                      }
                    }

                    return Column(
                      children: spaces,
                    );
                  }),
                  const SizedBox(
                    height: 164,
                  ),
                ],
              )
            ],
          )),
    );
  }
}
