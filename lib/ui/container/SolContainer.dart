import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/domain/core/entity/task/TaskCreateState.dart';
import 'package:sol_app/ui/components/bottomPanel/BottomPanel.dart';
import 'package:sol_app/ui/components/buttons/TaskAddButton.dart';
import 'package:sol_app/ui/components/createComponent/CloseBackground.dart';
import 'package:sol_app/ui/components/createComponent/CreateComponent.dart';

class SolContainer extends StatelessWidget {
  const SolContainer(this.child, {Key? key}) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, childD) {
      return Stack(
        alignment: AlignmentDirectional.topStart,
        children: [
          child,
          domain.createForm.state == TaskCreateState.none
              ? TaskAddButton(() {
                  domain.createForm.showForm();
                })
              : Container(),
          domain.createForm.state == TaskCreateState.none
              ? const BottomPanel()
              : Container(),
          domain.createForm.state == TaskCreateState.createForm
              ? CloseBackground(() {
                  domain.createForm.hideForm();
                })
              : Container(),
          domain.createForm.state == TaskCreateState.createForm
              ? const CreateComponent()
              : Container()
        ],
      );
    });
  }
}
