import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/components/eventModal/ModalPin.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class DeadlineChange extends StatelessWidget {
  const DeadlineChange(this.initialDate, this.onDateTimeChanged, this.onDeleteDate, {Key? key}) : super(key: key);
  final DateTime initialDate;
  final ValueChanged<DateTime> onDateTimeChanged;
  final VoidCallback onDeleteDate;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
        height: 320,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: const [Spacer(), ModalPin(), Spacer()],
            ),
            const SizedBox(
              width: 1,
              height: 8,
            ),
            Text(
              'Change deadline for task',
              style: TextStyle(
                  color: SolColors.current.text.primary,
                  fontSize: 18,
                  fontWeight: FontWeight.w600),
            ),
            const Spacer(),
            Container(
              // width: 100,
              height: 180,
              child: CupertinoDatePicker(
                initialDateTime: initialDate,
                mode: CupertinoDatePickerMode.dateAndTime,
                use24hFormat: true,
                // This is called when the user changes the dateTime.
                onDateTimeChanged: onDateTimeChanged,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              children: [
                const Spacer(),
                TextButton(
                  onPressed: onDeleteDate,
                  child: Text(
                    'Delete Deadline',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: SolColors.current.text.error),
                  ),
                ),
                const Spacer(),
              ],
            ),


            // Тут в конце кнопку удалить дедлайн нужно сделать
            // А еще сделать реализацию сделать в других
            // И отображение и отправку сделать
            // Так же сделать отправку данныхbuild(context)
          ],
        ));
  }
}
