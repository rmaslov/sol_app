import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/domain/core/entity/task/TaskEntity.dart';
import 'package:sol_app/infrastructure/api/task/response/TaskResult.dart';
import 'package:sol_app/infrastructure/utils/StringUtils.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class SpaceTaskItem extends StatelessWidget {
  const SpaceTaskItem(this.taskEntity, {Key? key}) : super(key: key);
  final TaskEntity taskEntity;

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child){
      return Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: TextButton(
            onPressed: () {

            },
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                      side: BorderSide(
                          color:
                          SolColors.current.spaceItem.backgroundToggleOpen))),
              foregroundColor:
              MaterialStateProperty.all(SolColors.current.text.primary),
              padding: MaterialStateProperty.all(
                  const EdgeInsets.fromLTRB(0, 0, 0, 8)),
              backgroundColor: MaterialStateProperty.all(
                  SolColors.current.spaceItem.backgroundToggleOpen),
              // MaterialStateProperty.all(SolColors.current.spaceItem.backgroundToggleOpen),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: EdgeInsets.all(0.0),
                    width: 42,
                    height: 42,
                    alignment: Alignment.topLeft,
                    child: TextButton(
                        onPressed: () {
                          domain.tasksStore.makeDoneOrOpen(taskEntity.taskResult.id!);
                        },
                        child: Image.asset(
                          TaskStatus.DONE == taskEntity.taskResult.status
                              ? 'assets/ic_checkbox_close.png'
                              :
                          'assets/ic_checkbox_open.png',
                        ))),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: MediaQuery.of(context).size.width - 4 - 42 - 16,
                        padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                        child: Text(
                          taskEntity.taskResult.iconAndTitle(),
                          softWrap: true,
                          maxLines: 18,
                          style: TextStyle(
                              overflow: TextOverflow.ellipsis,
                              fontSize: 16,
                              decoration: TaskStatus.DONE == taskEntity.taskResult.status
                                  ? TextDecoration.lineThrough
                                  : TextDecoration.none,
                              color: TaskStatus.DONE == taskEntity.taskResult.status
                                  ? SolColors.current.text.placeholder
                                  : SolColors.current.text.primary,
                              fontWeight: FontWeight.w400),
                        )),
                    Text(
                      '1pt • 12.12.2021',
                      style: TextStyle(
                          fontSize: 12,
                          decoration: TaskStatus.DONE == taskEntity.taskResult.status
                              ? TextDecoration.lineThrough
                              : TextDecoration.none,
                          fontWeight: FontWeight.w400,
                          color: SolColors.current.text.placeholder),
                    )
                  ],
                ),
                const SizedBox(
                  width: 0,
                ),
              ],
            ),
          ));
    });
  }
}
