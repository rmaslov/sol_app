import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/domain/core/entity/space/SpaceEntity.dart';
import 'package:sol_app/infrastructure/utils/StringUtils.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class SpaceItem extends StatelessWidget {
  SpaceItem(this.spaceEntity, {Key? key, this.onClickToggle}) : super(key: key);
  final SpaceEntity spaceEntity;
  VoidCallback? onClickToggle;

  Color background() {
    return spaceEntity.spaceView.showTasksOnGeneralScreen
        ? SolColors.current.spaceItem.backgroundToggleOpen
        : SolColors.current.spaceItem.backgroundToggleClose;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextButton(
            onPressed: () {},
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(12),
                          topRight: const Radius.circular(12),
                          bottomLeft: Radius.circular(
                              spaceEntity.spaceView.showTasksOnGeneralScreen
                                  ? 0
                                  : 12),
                          bottomRight: Radius.circular(
                              spaceEntity.spaceView.showTasksOnGeneralScreen
                                  ? 0
                                  : 12)),
                      side: BorderSide(color: background()))),
              foregroundColor:
                  MaterialStateProperty.all(SolColors.current.text.primary),
              padding: MaterialStateProperty.all(const EdgeInsets.all(0)),
              backgroundColor: MaterialStateProperty.all(background()),
            ),
            child: Row(
              children: [
                const SizedBox(
                  width: 16,
                ),
                Container(
                  child: Text(
                    safe(spaceEntity.spaceResult.icon?.data),
                    style: TextStyle(
                        fontSize: 16,
                        color: SolColors.current.text.primary,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                const SizedBox(
                  width: 12,
                ),
                Text(
                  safe(spaceEntity.spaceResult.title),
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.w400),
                ),
                const Spacer(),
                Container(
                    width: 48,
                    height: 48,
                    alignment: Alignment.centerRight,
                    child: TextButton(
                        style: const ButtonStyle(),
                        onPressed: onClickToggle,
                        child: Image.asset(
                          spaceEntity.spaceView.showTasksOnGeneralScreen
                              ? 'assets/ic_toggle_close.png'
                              : 'assets/ic_toggle_open.png',
                          width: 24,
                          height: 24,
                        ))),
                const SizedBox(
                  width: 2,
                ),
              ],
            )),
      )
    ]);
  }
}
