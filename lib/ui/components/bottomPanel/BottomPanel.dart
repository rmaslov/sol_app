import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:sol_app/ui/components/bottomPanel/BottomPanelGeneral.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class BottomPanel extends StatelessWidget {
  const BottomPanel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 54,
      right: 0,
      left: 0,
      height: 48,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: Container(
          height: 48,
          alignment: Alignment.topCenter,
          decoration: BoxDecoration(
              color: SolColors.current.bottomPanel.background,
              borderRadius: const BorderRadius.all(Radius.circular(8))),
          child: BottomPanelGeneral(),
        ),
      ),
    );
  }
}
