import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/bottomPanel/ButtonForPanel.dart';
import 'package:sol_app/ui/components/calendar/CalendarOpenFrom.dart';
import 'package:sol_app/ui/pages/calendar/CalendarPage.dart';

class BottomPanelGeneral extends StatelessWidget {
  const BottomPanelGeneral({Key? key}) : super(key: key);

  void expandPressed(BuildContext context) {
    CoreDomain coreDomain = Provider.of<CoreDomain>(context, listen: false);
    coreDomain.spacesStore.toggleAllSpaces(
        !coreDomain.spacesStore.spaces[0].spaceView.showTasksOnGeneralScreen);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Spacer(),
        Consumer<CoreDomain>(
          builder: (context, domain, child) {
            return ButtonForPanel(
                (domain.spacesStore.spaces.isNotEmpty  && domain.spacesStore.spaces[0].spaceView
                        .showTasksOnGeneralScreen == true)
                    ? 'assets/ic_toggle_all_close_day.png'
                    : 'assets/ic_toggle_all_open_day.png',
                'Toggle all', () {
              expandPressed(context);
            });
          },
        ),
        const Spacer(),
        ButtonForPanel('assets/ic_notification_white.png', 'Notification', () {
          // Navigator.pushNamed(context, Routes.calendar);
          showCupertinoModalBottomSheet<void>(
            context: context,
            expand: true,
            builder: (BuildContext context) {
              return const CalendarPage(CalendarOpenFrom.fromDefault);
            },
          );
        }),
        const Spacer(),
        ButtonForPanel('assets/ic_calendar_white.png', 'Planning', () {
          // Navigator.pushNamed(context, Routes.calendar);
          showCupertinoModalBottomSheet<void>(
            context: context,
            expand: true,
            builder: (BuildContext context) {
              return const CalendarPage(CalendarOpenFrom.fromDefault);
            },
          );
        }),
        const Spacer(),
      ],
    );
  }
}
