import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:sol_app/ui/components/bottomPanel/BottomPanelGeneral.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class ButtonForPanel extends StatelessWidget {
  const ButtonForPanel(this.icon, this.title, this.onPressed, {Key? key})
      : super(key: key);
  final String title;
  final String icon;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.all(const Size.fromWidth(64)),
            padding: MaterialStateProperty.all(
                const EdgeInsets.fromLTRB(8, 0, 8, 0))),
        onPressed: onPressed,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              icon,
              width: 24,
              height: 24,
            ),
            Text(title,
                style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                    color: SolColors.current.bottomPanel.textColor))
          ],
        ));
  }
}
