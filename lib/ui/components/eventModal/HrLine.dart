import 'package:flutter/cupertino.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class HrLine extends StatelessWidget {
  const HrLine( {Key? key, this.color}) : super(key: key);
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1,
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(0),
          color: color ?? SolColors.current.hr.hrLine),
    );
  }
}
