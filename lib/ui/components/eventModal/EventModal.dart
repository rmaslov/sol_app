import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/domain/core/entity/meeting/Meeting.dart';
import 'package:sol_app/domain/core/entity/meeting/MeetingType.dart';
import 'package:sol_app/ui/components/eventModal/HrLine.dart';
import 'package:sol_app/ui/components/eventModal/ModalPin.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class EventModal extends StatelessWidget {
  const EventModal(this.meeting, {Key? key}) : super(key: key);
  final Meeting meeting;

  // Тут нужно дальше сделать так, что бы подробности meeting
  // нужно еще сделать получешие/выбор views

  void onTap(BuildContext context) {
    if (meeting.type == MeetingType.draft) {
      Navigator.pop(context);
    }
    if (meeting.type == MeetingType.slot) {}
    if (meeting.type == MeetingType.eventCalendar) {}
  }

  void onDelete(BuildContext context, CoreDomain domain) {
    if (meeting.type == MeetingType.draft) {
      domain.slotStore.deleteDraft(meeting);
      Navigator.pop(context);
    }
    if (meeting.type == MeetingType.slot) {
      domain.slotStore.deleteSlot(meeting);
      Navigator.pop(context);
    }
    if (meeting.type == MeetingType.eventCalendar) {}
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child){
      return Container(
          padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
          height: 250,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: const [Spacer(), ModalPin(), Spacer()],
              ),
              const SizedBox(
                width: 1,
                height: 8,
              ),
              Text(
                meeting.eventName,
                style: TextStyle(
                    color: SolColors.current.text.primary,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
              const SizedBox(
                width: 1,
                height: 8,
              ),
              Text(
                meeting.prettyDate(),
                style: TextStyle(
                    fontSize: 12, color: SolColors.current.text.placeholder),
              ),
              const SizedBox(
                width: 1,
                height: 16,
              ),
              HrLine(),
              const SizedBox(
                width: 1,
                height: 0,
              ),
              SizedBox(

                height: 32,
                width: double.infinity,
                child: TextButton(
                  style: ButtonStyle(
                    alignment: Alignment.centerLeft,
                    padding: MaterialStateProperty.all(EdgeInsets.zero),
                  ),
                  onPressed: () => onTap(context),
                  child: Text(
                    meeting.external(),
                    style: TextStyle(
                        fontSize: 12, color: SolColors.current.text.primary),
                  ),
                ),
              ),
              const SizedBox(
                width: 1,
                height: 0,
              ),
              HrLine(),
              Spacer(),
              Row(
                children: [
                  Spacer(),
                  TextButton(
                    onPressed: () => onDelete(context, domain),
                    child: Text(
                      'Delete Event',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: SolColors.current.text.error),
                    ),
                  ),
                  Spacer(),
                ],
              ),
              const SizedBox(
                width: 1,
                height: 24,
              ),
            ],
          ));
    },) ;
  }
}
