import 'package:flutter/cupertino.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class ModalPin extends StatelessWidget {
  const ModalPin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 5,
      width: 24,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: SolColors.current.button.background,
      ),
    );
  }
}
