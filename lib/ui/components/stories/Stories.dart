import 'package:flutter/cupertino.dart';

import 'StoryButton.dart';
import 'StoryConfigButton.dart';

class Stories extends StatefulWidget {
  const Stories({Key? key}) : super(key: key);

  @override
  State<Stories> createState() => _StoriesState();
}

class _StoriesState extends State<Stories> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 0.0),
        width: null,
        height: 78.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            SizedBox(width: 10),
            StoryButton('🩳', 'new asdf asdf asdf ', true, 1),
            SizedBox(width: 4),
            StoryButton('🩳', 'new', false, 123),
            SizedBox(width: 4),
            StoryButton('🩳', 'new', false, 1231),
            SizedBox(width: 4),
            StoryButton('🩳', 'new', true, 1231),
            SizedBox(width: 4),
            StoryButton('🩳', 'new', false, 1231),
            SizedBox(width: 4),
            StoryButton('🩳', 'new', true, 1231),
            SizedBox(width: 4),
            StoryButton('🩳', 'new', false, 1231),
            SizedBox(width: 4),
            StoryButton('🩳', 'new', false, 1231),
            SizedBox(width: 4),
            StoryConfigButton(),
            SizedBox(width: 16,),

          ],
        ));
  }
}
