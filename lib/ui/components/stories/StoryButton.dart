import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class StoryButton extends StatelessWidget {
  const StoryButton(this.icon, this.title, this.hasNewTask, this.count,
      {Key? key})
      : super(key: key);

  final bool hasNewTask;
  final String icon;
  final String title;
  final int count;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 56,
        height: 10,
        child: TextButton(
            style: ButtonStyle(
              padding: MaterialStateProperty.all(EdgeInsets.zero),
            ),
            onPressed: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 52,
                      height: 48,
                    ),
                    Container(
                      width: 48,
                      height: 48,
                      decoration:  BoxDecoration(
                        color: hasNewTask
                            ? SolColors.current.story.hasNewTask
                            : SolColors.current.story.hasNotNewTask,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Container(
                      width: 45,
                      height: 45,
                      decoration: new BoxDecoration(
                        color: SolColors.current.story.space,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Container(
                      width: 42,
                      height: 42,
                      decoration:  BoxDecoration(
                        color: SolColors.current.story.background,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Text(
                      this.icon,
                      style: TextStyle(fontSize: 24),
                    ),
                    Positioned(
                      left: 30,
                      top: 0,
                      child: Container(
                        width: 22,
                        height: 22,
                        alignment: Alignment.center,
                        decoration:  BoxDecoration(
                          color: hasNewTask
                              ? SolColors.current.story.countHasBackground
                              : SolColors.current.story.countHasNotBackground,
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          '${this.count}',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: (count < 100
                                  ? 12
                                  : (count < 1000
                                  ? 9
                                  : (count < 1000 ? 8 : 6))),
                              color: hasNewTask
                                  ? SolColors.current.story.countHasText
                                  : SolColors.current.story.countHasNotText),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 2,
                ),
                Container(
                  width: 56,
                  alignment: Alignment.center,
                  child: Text(
                    this.title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 12, color: SolColors.current.story.text),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                )
              ],
            )));
  }
}

