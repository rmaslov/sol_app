
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class StoryConfigButton extends StatelessWidget {
  const StoryConfigButton(
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 56,
        height: 10,
        child: TextButton(
            style: ButtonStyle(
              padding: MaterialStateProperty.all(EdgeInsets.zero),
            ),
            onPressed: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 52,
                      height: 48,
                    ),
                    Container(
                      width: 48,
                      height: 48,
                      decoration: BoxDecoration(
                        color: SolColors.current.story.hasNewTask,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Container(
                      width: 45,
                      height: 45,
                      decoration: new BoxDecoration(
                        color: SolColors.current.story.space,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Container(
                      width: 42,
                      height: 42,
                      decoration: BoxDecoration(
                        color: SolColors.current.story.background,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Image.asset('assets/ic_more.png', width: 24,height: 24,)
                  ],
                ),
              ],
            )));
  }
}
