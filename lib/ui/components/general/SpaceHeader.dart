import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class SpaceHeader extends StatelessWidget {
  const SpaceHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 12,
        ),
        Text(
          'Spaces',
          style: TextStyle(
              fontSize: 16,
              color: SolColors.current.text.primary,
              fontWeight: FontWeight.w500),
        ),
        Spacer(),
        TextButton(
            onPressed: () {
              showCupertinoModalBottomSheet<void>(
                context: context,
                builder: (BuildContext context) {
                  return Container(
                    //height: 200,
                    color: Colors.amber,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          const Text('Modal BottomSheet'),
                          ElevatedButton(
                            child: const Text('Close BottomSheet'),
                            onPressed: () => Navigator.pop(context),
                          )
                        ],
                      ),
                    ),
                  );
                },
              );
            },
            style: ButtonStyle(
                alignment: Alignment.centerRight,
                foregroundColor: MaterialStateProperty.all(SolColors.current.button.primary)),
            child: Text('+')),
        SizedBox(
          width: 12,
        ),
      ],
    );
  }
}
