import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class GeneralHeader extends StatefulWidget {
  const GeneralHeader({Key? key}) : super(key: key);

  @override
  State<GeneralHeader> createState() => _GeneralHeaderState();
}

class _GeneralHeaderState extends State<GeneralHeader> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "Sol",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  color: SolColors.current.text.primary),
            ),
            const Spacer(),
            TextButton(
                style: const ButtonStyle(alignment: Alignment.centerRight),
                onPressed: () {},
                child: Icon(
                  CupertinoIcons.settings,
                  size: 24,
                  color: SolColors.current.text.primary,
                )),
          ],
        ));
  }
}
