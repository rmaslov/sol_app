import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/ui/components/buttons/NotificationButton.dart';
import 'package:sol_app/ui/components/buttons/SolButton.dart';
import 'package:sol_app/ui/components/eventModal/HrLine.dart';
import 'package:sol_app/ui/components/eventModal/ModalPin.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class NotificationChange extends StatefulWidget {
  const NotificationChange(
      this.dates, this.onAddedNotification, this.onDeleteDate,
      {Key? key})
      : super(key: key);
  final List<DateTime> dates;
  final ValueChanged<DateTime> onAddedNotification;
  final ValueChanged<DateTime> onDeleteDate;

  @override
  State<NotificationChange> createState() => _NotificationChangeState();
}

class _NotificationChangeState extends State<NotificationChange> {
  DateTime dateTime = DateTime.now().add(Duration(days: 1));
  List<DateTime> dates = [];

  // @override
  // void didChangeDependencies() {
  //   super.didChangeDependencies();
  //   final someClass = Provider.of<List<DateTime>>(context);
  //   build(context);
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
        //padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 8,
        ),
        Row(
          children: const [Spacer(), ModalPin(), Spacer()],
        ),
        const SizedBox(
          width: 1,
          height: 8,
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 10),
          child: Row(
            children: [
              const Spacer(),
              Text(
                'Change Notifications',
                style: TextStyle(
                    color: SolColors.current.text.primary,
                    fontSize: 18,
                    fontWeight: FontWeight.w600),
              ),
              const Spacer()
            ],
          ),
        ),
        HrLine(
          color: SolColors.current.hr.hrLineLite,
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
          height: MediaQuery.of(context).size.height - 305,
          child: widget.dates.length > 0 ? ListView(
            children: widget
                .dates
                .map((e) => Column(
                      children: [
                        SizedBox(
                          height: 8,
                        ),
                        NotificationButton(DateFormat('MMM dd hh:mm a').format(e), () {
                          widget.onDeleteDate(e);
                        }),
                        SizedBox(
                          height: 8,
                        )
                      ],
                    ))
                .toList(),
          ) : Column(children: [
            Spacer(),
            Row(children: [
              Spacer(),
              Text('No Notification'),

              Spacer(),
            ],),
            Spacer(),
          ],),
        ),
        HrLine(
          color: SolColors.current.hr.hrLineLite,
        ),
        Container(
          height: 150,
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: CupertinoDatePicker(
            initialDateTime: dateTime,
            mode: CupertinoDatePickerMode.dateAndTime,
            use24hFormat: true,
            // This is called when the user changes the dateTime.
            onDateTimeChanged: (date) {
              setState(() {
                dateTime = date;
              });
            },
          ),
        ),
        SizedBox(
          height: 4,
        ),
        Container(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: SolButton('Add notification', onPressed: () {
              this.widget.onAddedNotification(dateTime);
            })),
      ],
    ));
  }
}
