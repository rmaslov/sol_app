import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/components/buttons/ButtonState.dart';
import 'package:sol_app/ui/components/buttons/styles/DefaultButtonStyle.dart';
import 'package:sol_app/ui/components/buttons/styles/DisableButtonStyle.dart';

class SolButton extends StatelessWidget {
  const SolButton(this.title,
      {Key? key, this.onPressed, this.state = ButtonState.defaultState})
      : super(key: key);
  final String title;
  final VoidCallback? onPressed;
  final ButtonState state;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: ((state != ButtonState.loading && state != ButtonState.disable)
          ? onPressed
          : null),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Spacer(),
          Text(title),
          Spacer(),
        ],
      ),
      style: (state != ButtonState.loading && state != ButtonState.disable)
          ? DefaultButtonStyle
          : DisableButtonStyle,
    );
  }
}
