import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/sol_app_router.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class EmailButton extends StatelessWidget {
  const EmailButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        Navigator.pushNamed(context, Routes.loginByEmail);
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            width: 4,
          ),
          Image.asset(
            'assets/ic_mail.png',
            width: 24,
            height: 24,
          ),
          const Spacer(),
          const Text('Email'),
          const Spacer(),
          const SizedBox(
            width: 28,
          )
        ],
      ),
      style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  side: BorderSide(
                      color: SolColors.current.button.emailBackground))),
          textStyle: MaterialStateProperty.all(
              const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          minimumSize: MaterialStateProperty.all(const Size.fromHeight(44.0)),
          backgroundColor: MaterialStateProperty.all(
              SolColors.current.button.emailBackground),
          foregroundColor:
              MaterialStateProperty.all(SolColors.current.button.emailText)),
    );
  }
}
