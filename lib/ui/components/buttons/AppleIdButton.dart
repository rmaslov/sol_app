import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class AppleIdButton extends StatelessWidget {
  const AppleIdButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {},
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            width: 8,
          ),
          Image.asset(
            'assets/ic_ios.png',
            width: 12,
            height: 15,
          ),
          const Spacer(),
          const Text('Apple ID'),
          const Spacer(),
          const SizedBox(
            width: 20,
          )
        ],
      ),
      style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  side: BorderSide(
                      color: SolColors.current.button.appleBackground))),
          textStyle: MaterialStateProperty.all(
              const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          minimumSize: MaterialStateProperty.all(const Size.fromHeight(44.0)),
          backgroundColor: MaterialStateProperty.all(
              SolColors.current.button.appleBackground),
          foregroundColor: MaterialStateProperty.all(Colors.white)),
    );
  }
}
