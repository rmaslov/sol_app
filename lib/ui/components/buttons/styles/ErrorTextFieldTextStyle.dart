import 'package:flutter/cupertino.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

TextStyle ErrorTextFieldTextStyle =
    TextStyle(fontSize: 16, color: SolColors.current.field.textError);
