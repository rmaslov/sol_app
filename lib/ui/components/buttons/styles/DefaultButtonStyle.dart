import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

ButtonStyle DefaultButtonStyle = ButtonStyle(
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: BorderSide(color: SolColors.current.button.primary))),
    textStyle: MaterialStateProperty.all(
        const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
    minimumSize: MaterialStateProperty.all(const Size.fromHeight(44.0)),
    backgroundColor:
        MaterialStateProperty.all(SolColors.current.button.primary),
    foregroundColor: MaterialStateProperty.all(SolColors.current.button.text));
