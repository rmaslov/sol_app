import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class TaskAddButton extends StatelessWidget {
  const TaskAddButton(this.onPressed, {Key? key}) : super(key: key);
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Positioned(
        bottom: 118,
        right: 16,
        child: SizedBox(
            width: 48,
            height: 48,
            child: TextButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.zero),
                ),
                onPressed: () {
                  onPressed();

                  // showCupertinoModalBottomSheet<void>(
                  //   context: context,
                  //   builder: (BuildContext context) {
                  //     return Container(
                  //       height: 200,
                  //       color: Colors.amber,
                  //       child: Center(
                  //         child: Column(
                  //           mainAxisAlignment: MainAxisAlignment.center,
                  //           mainAxisSize: MainAxisSize.min,
                  //           children: <Widget>[
                  //             const Text('Modal BottomSheet'),
                  //             ElevatedButton(
                  //               child: const Text('Close BottomSheet'),
                  //               onPressed: () => Navigator.pop(context),
                  //             )
                  //           ],
                  //         ),
                  //       ),
                  //     );
                  //   },
                  // );

                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width: 48,
                          height: 48,
                          decoration: BoxDecoration(
                            color: SolColors.current.story.hasNewTask,
                            shape: BoxShape.circle,
                          ),
                        ),
                        Image.asset(
                          'assets/ic_plus.png',
                          width: 24,
                          height: 24,
                        )
                      ],
                    ),
                  ],
                ))));
  }
}
