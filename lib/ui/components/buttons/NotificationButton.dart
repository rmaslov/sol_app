import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/components/buttons/ButtonState.dart';
import 'package:sol_app/ui/components/buttons/styles/DefaultButtonStyle.dart';
import 'package:sol_app/ui/components/buttons/styles/DisableButtonStyle.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class NotificationButton extends StatelessWidget {
  const NotificationButton(
    this.title,
    this.onDelete, {
    Key? key,
  }) : super(key: key);

  final String title;
  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return Container(
      //style:DefaultButtonStyle
      height: 41,
      width: double.infinity,
      decoration: BoxDecoration(
        color: SolColors.current.button.primary,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 8,
          ),
          Text(
            title,
            style: TextStyle(color: SolColors.current.button.text),
          ),
          Spacer(),
          SizedBox(
            child: TextButton(
                style: ButtonStyle(alignment: Alignment.centerRight),
                onPressed: onDelete,
                child: Image.asset(
                  'assets/ic_close_white.png',
                  width: 24,
                  height: 24,
                )),
          )
        ],
      ),
    );
  }
}
