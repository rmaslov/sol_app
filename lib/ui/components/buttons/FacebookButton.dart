import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FacebookButton extends StatelessWidget {
  const FacebookButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {},
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            width: 4,
          ),
          Image.asset(
            'assets/ic_facebook.png',
            width: 24,
            height: 24,
          ),
          const Spacer(),
          const Text('Facebook'),
          const Spacer(),
          const SizedBox(
            width: 28,
          )
        ],
      ),
      style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  side: const BorderSide(
                      color:Color.fromRGBO(66, 103, 178, 1)))),
          textStyle: MaterialStateProperty.all(
              const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          minimumSize: MaterialStateProperty.all(const Size.fromHeight(44.0)),
          backgroundColor:
              MaterialStateProperty.all(const Color.fromRGBO(66, 103, 178, 1)),
          foregroundColor: MaterialStateProperty.all(Colors.white)),
    );
  }
}
