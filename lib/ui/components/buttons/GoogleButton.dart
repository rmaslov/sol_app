import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GoogleButton extends StatelessWidget {
  const GoogleButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {},
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            width: 4,
          ),
          Image.asset(
            'assets/ic_google.png',
            width: 24,
            height: 24,
          ),
          const Spacer(),
          const Text('Apple ID'),
          const Spacer(),
          const SizedBox(
            width: 28,
          )
        ],
      ),
      style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  side: const BorderSide(
                      color: Color.fromRGBO(234, 67, 53, 1)))),
          textStyle: MaterialStateProperty.all(
              const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
          minimumSize: MaterialStateProperty.all(const Size.fromHeight(44.0)),
          backgroundColor:
              MaterialStateProperty.all(const  Color.fromRGBO(234, 67, 53, 1)),
          foregroundColor: MaterialStateProperty.all(Colors.white)),
    );
  }
}
