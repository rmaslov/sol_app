import 'package:flutter/cupertino.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class LoginHeaderText extends StatelessWidget {

  const LoginHeaderText(this.text, {Key? key}) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontSize: 32,
          fontWeight: FontWeight.w700,
          color: SolColors.current.text.primary),
    );
  }
}

