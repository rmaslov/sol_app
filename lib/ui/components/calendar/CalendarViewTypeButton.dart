import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class CalendarViewTypeButton extends StatelessWidget {
  const CalendarViewTypeButton(this.title,this.select, this.onPressed, {Key? key}) : super(key: key);
  final String title;
  final bool select;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32,
      width: double.infinity,
      child: TextButton(
        style: ButtonStyle(
          alignment: Alignment.centerLeft,
          padding: MaterialStateProperty.all(EdgeInsets.zero),
        ),
        onPressed: (){
          onPressed();
        },
        child: Text(
          title,
          style: TextStyle(
              fontSize: 12,
              fontWeight: select ? FontWeight.w800: FontWeight.w500,
              color: SolColors.current.text.primary),
        ),
      ),
    );
  }
}
