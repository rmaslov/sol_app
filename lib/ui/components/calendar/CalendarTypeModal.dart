import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/eventModal/HrLine.dart';
import 'package:sol_app/ui/components/eventModal/ModalPin.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class EventModal extends StatelessWidget {
  const EventModal( {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child){
      return Container(
          padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
          height: 200,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: const [Spacer(), ModalPin(), Spacer()],
              ),
              const SizedBox(
                width: 1,
                height: 8,
              ),

              SizedBox(
                height: 32,
                width: double.infinity,
                child: TextButton(
                  style: ButtonStyle(
                    alignment: Alignment.centerLeft,
                    padding: MaterialStateProperty.all(EdgeInsets.zero),
                  ),
                  onPressed: () => {},
                  child: Text(
                    "Day",
                    style: TextStyle(
                        fontSize: 12, color: SolColors.current.text.primary),
                  ),
                ),
              ),

            ],
          ));
    },) ;
  }
}
