import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/domain/core/entity/meeting/Meeting.dart';
import 'package:sol_app/domain/core/entity/meeting/MeetingDataSource.dart';
import 'package:sol_app/ui/components/calendar/CalendarOpenFrom.dart';
import 'package:sol_app/ui/components/calendar/EventDetailWindow.dart';
import 'package:sol_app/ui/components/eventModal/EventModal.dart';
import 'package:sol_app/ui/components/eventModal/ModalPin.dart';
import 'package:sol_app/ui/styles/SolColors.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';



/// The hove page which hosts the calendar
class CalendarComponent extends StatefulWidget {
  /// Creates the home page to display teh calendar widget.
  const CalendarComponent(this.openFrom, {Key? key}) : super(key: key);
  final CalendarOpenFrom openFrom;

  @override
  // ignore: library_private_types_in_public_api
  _CalendarComponentState createState() => _CalendarComponentState();
}

class _CalendarComponentState extends State<CalendarComponent> {
  //MeetingDataSource dataSource = MeetingDataSource(_getDataSource());
  CalendarView? calendarView;

  @override
  void initState() {
    calendarView = CalendarView.day;

    Future.delayed(Duration.zero, () {
      CoreDomain domain = Provider.of<CoreDomain>(context, listen: false);
      domain.slotStore.loadSlots(DateTime.now());
    });
  }

  void addDraft(CalendarTapDetails calendarTapDetails, CoreDomain domain) {
    domain.slotStore.addDraft(
        domain.createForm.request.titleWithIcon(),
        calendarTapDetails.date!,
        calendarTapDetails.date!.add(Duration(hours: 1)),
        false);
  }

  void openEvent(CalendarTapDetails calendarTapDetails, CoreDomain domain) {
    for (var app in calendarTapDetails.appointments!) {
      if (app is Meeting) {
        Meeting meeting = app as Meeting;

        showCupertinoModalBottomSheet<void>(
          context: context,
          expand: false,
          builder: (BuildContext context) {
            return EventModal(meeting);
          },
        );
        return;
      }
    }
  }

  void onTap(CalendarTapDetails calendarTapDetails, CoreDomain domain) {
    if (calendarTapDetails.date == null) return;
    if (calendarView == CalendarView.month) return;

    if (calendarTapDetails.targetElement.name == "calendarCell" && widget.openFrom == CalendarOpenFrom.fromTaskCreate) {
      addDraft(calendarTapDetails, domain);
    }
    if (calendarTapDetails.targetElement.name == "appointment" &&
        calendarTapDetails.appointments != null) {
      openEvent(calendarTapDetails, domain);
    }
  }

  void onDragEnd(
      AppointmentDragEndDetails appointmentDragEndDetails, CoreDomain domain) {
    print(appointmentDragEndDetails.droppingTime);
    if (appointmentDragEndDetails.appointment is Meeting) {
      Meeting meeting = appointmentDragEndDetails.appointment as Meeting;
      print(meeting.id);
      domain.slotStore
          .updateTime(meeting, appointmentDragEndDetails.droppingTime!);
    }
  }

  void onLongPress(
      CalendarLongPressDetails calendarLongPressDetails, CoreDomain domain) {}

  void onClose() {
    Navigator.pop(context);
  }

  void onTapView() {
    showCupertinoModalBottomSheet<void>(
      context: context,
      expand: false,
      builder: (BuildContext context) {
        return EventDetailWindow();
      },
    );
  }

  String viewTypeTitle(CalendarView calendarView) {
    if (calendarView == CalendarView.day) return 'View: Day';
    if (calendarView == CalendarView.week) return 'View: Week';
    if (calendarView == CalendarView.month) return 'View: Month';
    if (calendarView == CalendarView.timelineWeek) return 'View: Timeline';
    if (calendarView == CalendarView.schedule) return 'View: Schedule';
    return 'Change view';
  }

  Widget calendar(CoreDomain domain) {
    if (domain.slotStore.calendarView != calendarView) {
      Future.delayed(Duration(milliseconds: 1), () {
        setState(() {
          calendarView = domain.slotStore.calendarView;
        });
      });
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (content, domain, child) {
      return CupertinoPageScaffold(
        backgroundColor: SolColors.current.background.primary,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        SizedBox(
          height: 4,
        ),
        Row(
          children: [
            Spacer(),
            ModalPin(),
            Spacer(),
          ],
        ),
        Expanded(
            child: domain.slotStore.calendarView == calendarView
                ? SfCalendar(
                    view: domain.slotStore.calendarView,
                    dataSource: MeetingDataSource(domain.slotStore.meetings),
                    onLongPress:
                        (CalendarLongPressDetails calendarLongPressDetails) =>
                            onLongPress(calendarLongPressDetails, domain),
                    allowDragAndDrop: true,
                    allowAppointmentResize: true,
                    onTap: (CalendarTapDetails calendarTapDetails) {
                      onTap(calendarTapDetails, domain);
                    },
                    onDragEnd:
                        (AppointmentDragEndDetails appointmentDragEndDetails) =>
                            onDragEnd(appointmentDragEndDetails, domain),
                  )
                : calendar(domain)),
        Container(
          color: SolColors.current.calendar.bottomHr,
          height: 1,
        ),
        Container(
          color: SolColors.current.calendar.bottomBackground,
          padding: EdgeInsets.fromLTRB(0, 0, 0, 32),
          child: Row(
            children: [
              TextButton(
                  onPressed: onClose,
                  child: Text(
                    'Close',
                    style: TextStyle(color: SolColors.current.text.primary),
                  )),
              Spacer(),
              TextButton(
                  onPressed: onTapView,
                  child: Text(
                    viewTypeTitle(domain.slotStore.calendarView),
                    style: TextStyle(color: SolColors.current.text.primary),
                  )),
            ],
          ),
        ),
      ]));
    });
  }
}
