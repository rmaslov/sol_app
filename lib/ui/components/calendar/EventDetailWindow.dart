import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/calendar/CalendarViewTypeButton.dart';
import 'package:sol_app/ui/components/eventModal/ModalPin.dart';
import 'package:sol_app/ui/styles/SolColors.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class EventDetailWindow extends StatelessWidget {
  const EventDetailWindow({Key? key}) : super(key: key);

  void onTap(CoreDomain domain) {}

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(
      builder: (context, domain, child) {
        return Container(
            padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
            height: 250,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: const [Spacer(), ModalPin(), Spacer()],
                ),
                const SizedBox(
                  width: 1,
                  height: 8,
                ),
                Text(
                  'Calendar view type',
                  style: TextStyle(
                      color: SolColors.current.text.primary,
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  width: 1,
                  height: 8,
                ),
                CalendarViewTypeButton('Day', domain.slotStore.calendarView == CalendarView.day, () {
                  domain.slotStore.changeCalendarType(CalendarView.day);
                  Navigator.pop(context);
                }),
                CalendarViewTypeButton('Week', domain.slotStore.calendarView == CalendarView.week,() {
                  domain.slotStore.changeCalendarType(CalendarView.week);
                  Navigator.pop(context);
                }),
                CalendarViewTypeButton('Month', domain.slotStore.calendarView == CalendarView.month,() {
                  domain.slotStore.changeCalendarType(CalendarView.month);
                  Navigator.pop(context);
                }),
                CalendarViewTypeButton('Timeline month', domain.slotStore.calendarView == CalendarView.timelineMonth,() {
                  domain.slotStore
                      .changeCalendarType(CalendarView.timelineMonth);
                  Navigator.pop(context);
                }),
                CalendarViewTypeButton('Schedule', domain.slotStore.calendarView == CalendarView.schedule,() {
                  domain.slotStore.changeCalendarType(CalendarView.schedule);
                  Navigator.pop(context);
                }),
                const SizedBox(
                  width: 1,
                  height: 0,
                ),
                Spacer(),
              ],
            ));
      },
    );
  }
}
