import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';

import 'CreateZone.dart';

class CreateComponent extends StatefulWidget {
  const CreateComponent({Key? key}) : super(key: key);

  @override
  State<CreateComponent> createState() => _CreateComponentState();
}

class _CreateComponentState extends State<CreateComponent> {
  TextEditingController? textController;
  bool didLoad = false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      CoreDomain domain = Provider.of<CoreDomain>(context, listen: false);
      textController =
          TextEditingController(text: domain.createForm.request.title ?? '');

      textController?.addListener(() {
        if (didLoad == false) return;

        String check = textController!.text;
        if (check.isNotEmpty) {
          check = check.substring(check.length - 1);
          if (check == '\n') {
            domain.createTask(true);
            textController?.text = '';
          } else {
            domain.createForm.setTitleSilent(textController!.text, true);
          }
        } else {
          domain.createForm.setTitleSilent(textController!.text, true);
        }
      });

      setState(() {
        didLoad = true;
      });
    });
  }

  void textDidChange() {}

  @override
  Widget build(BuildContext context) {
    return didLoad ? CreateZone(textController) : Container();
  }
}
