import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:emojis/emoji.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/createComponent/EmojiLineButton.dart';
import 'package:sol_app/ui/components/createComponent/NoEmojiLineButton.dart';

class EmojiListButtons extends StatelessWidget {
  const EmojiListButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Set<String> justCheck = Set();
    List<Widget> emList = [];
    emList.add(const SizedBox(
      width: 8,
      height: 40,
    ));
    emList.add(NoEmojiLineButton());
    for (var element in Emoji.all()) {
      String stab = Emoji.stabilize(element.char);
      if (justCheck.contains(stab) == false) {
        emList.add(EmojiLineButton(element.char));
        justCheck.add(stab);
      }
    }
    emList.add(const SizedBox(
      width: 40,
      height: 40,
    ));

    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return SizedBox(
        height: 40,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: emList,
        ),
      );
    });
  }
}
