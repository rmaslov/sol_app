import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';

class EmojiLineButton extends StatelessWidget {
  const EmojiLineButton(this.icon,{Key? key})
      : super(key: key);
  final String icon;

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return SizedBox(
          width: 42,
          height: 42,
          child: TextButton(
              onPressed: () {
                domain.createForm.setEmoji(icon);
              },
              style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.zero)),
              child: Text(
                icon,
                style: const TextStyle(fontSize: 24),
              )));
    });
  }
}
