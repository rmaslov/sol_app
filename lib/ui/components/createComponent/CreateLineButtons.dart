import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/calendar/CalendarOpenFrom.dart';
import 'package:sol_app/ui/components/createComponent/CreateLineButtonWithText.dart';
import 'package:sol_app/ui/components/deadlineChange/DeadlineChange.dart';
import 'package:sol_app/ui/components/notificationChange/NotificationChange.dart';
import 'package:sol_app/ui/pages/calendar/CalendarPage.dart';

import 'CreateLineButton.dart';

class CreateLineButtons extends StatelessWidget {
  const CreateLineButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return Row(
        children: [
          Spacer(),
          (domain.createForm.request.icon?.data != null &&
                  domain.createForm.request.icon?.data != '')
              ? CreateLineButtonWithText(domain.createForm.request.icon!.data!,
                  () {
                  domain.createForm.toggleEmoji();
                })
              : CreateLineButton('assets/ic_smile.png', false, () {
                  domain.createForm.toggleEmoji();
                }),
          Spacer(),
          CreateLineButton(
              'assets/ic_calendar.png', domain.slotStore.drafts.length > 0, () {
            domain.slotStore
                .updateMeetingDrafts(domain.createForm.request.titleWithIcon());
            showCupertinoModalBottomSheet<void>(
              context: context,
              expand: true,
              builder: (BuildContext context) {
                return const CalendarPage(CalendarOpenFrom.fromTaskCreate);
              },
            );
            // Navigator.pushNamed(context, Routes.calendar);
          }),
          Spacer(),
          CreateLineButton('assets/ic_deadline.png',
              domain.createForm.request.deadline != null, () {
            domain.createForm.toggleDeadlines();
          }),
          Spacer(),
          CreateLineButton('assets/ic_notification.png', false, () {
            domain.createForm.hideAll();
            showCupertinoModalBottomSheet<void>(
              context: context,
              expand: true,
              builder: (BuildContext context) {
                return NotificationChange(
                    domain.createForm.request.reminders!
                        .map((e) => DateTime.fromMillisecondsSinceEpoch(e))
                        .toList(),
                    (date) {
                      domain.createForm.addNotification(date);
                    },
                    (date) {
                      domain.createForm.removeNotification(date);
                    },key: UniqueKey());
              },
            );
          }),
          Spacer(),
          CreateLineButton(
              'assets/ic_view.png', domain.createForm.views.isEmpty == false,
              () {
            domain.createForm.toggleViews();
          }),
          Spacer(),
          CreateLineButton(
              'assets/ic_space.png', domain.createForm.request.spaceId != null,
              () {
            domain.createForm.toggleSpaces();
          }),
          Spacer(),
        ],
      );
    });
  }
}
