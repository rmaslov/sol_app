import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/domain/core/entity/view/ViewEntity.dart';
import 'package:sol_app/infrastructure/api/view/response/AddedType.dart';
import 'package:sol_app/ui/components/createComponent/ViewLineButton.dart';

class ViewsListButtons extends StatelessWidget {
  const ViewsListButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child) {
      List<ViewEntity> filtered = [];
      for (var e in domain.viewStore.views) {
        if (e.viewResult.view!.addedType == AddedType.MANUALLY) {
          filtered.add(e);
        }
      }

      return SizedBox(
        height: 40,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: filtered
              .map((e) => ViewLineButton(e.viewResult,
                  domain.createForm.request.viewIds!.contains(e.viewResult.id)))
              .toList(),
        ),
      );
    });
  }
}
