import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class CreateLineButtonWithText extends StatelessWidget {
  const CreateLineButtonWithText(this.icon, this.onPressedIcon ,{Key? key}) : super(key: key);
  final String icon;
  final VoidCallback onPressedIcon;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 42,
        height: 42,
        child: TextButton(
            onPressed: onPressedIcon,
            style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.zero)),
            child: Text(icon, style: const TextStyle(
                fontSize: 18
            ),)));
  }
}
