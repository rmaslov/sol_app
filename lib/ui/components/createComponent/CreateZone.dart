import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/buttons/styles/DefaultTextFieldTextStyle.dart';
import 'package:sol_app/ui/components/createComponent/FastChooseDeadline.dart';
import 'package:sol_app/ui/components/createComponent/CreateLineButtons.dart';
import 'package:sol_app/ui/components/createComponent/EmojiListButtons.dart';
import 'package:sol_app/ui/components/createComponent/SpacesListButtons.dart';
import 'package:sol_app/ui/components/createComponent/ViewsListButtons.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

import 'AddButton.dart';

class CreateZone extends StatelessWidget {
  const CreateZone(this.textController, {Key? key}) : super(key: key);
  final TextEditingController? textController;

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return Positioned(
          bottom: MediaQuery.of(context).viewInsets.bottom,
          left: 0,
          right: 0,
          child: Column(children: [
            Container(
                color: SolColors.current.create.background,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: SolColors.current.create.hr,
                      height: 1,
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Row(
                      children: [
                        const SizedBox(
                          width: 8,
                        ),
                        Flexible(
                          child: CupertinoTextField(
                            controller: textController,
                            placeholder: '',
                            minLines: 1,
                            maxLines: 18,
                            autofocus: true,
                            style: DefaultTextFieldTextStyle,
                            padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                            keyboardType: TextInputType.multiline,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                    color: SolColors.current.create.border),
                                color: SolColors.current.create.background),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        AddButton(!domain.createForm.inProgress, () {
                          domain.createTask(false);
                        }),
                        SizedBox(
                          width: 8,
                        ),
                      ],
                    ),
                    domain.createForm.subtitle != ''
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 56, 0),
                            child: Text(
                                domain.createForm.subtitle,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: SolColors.current.create.subTitle,
                                    fontWeight: FontWeight.w400)),
                          )
                        : Container(),
                    const SizedBox(
                      height: 2,
                    ),
                    CreateLineButtons(),
                    domain.createForm.showSpacesList
                        ? SpacesListButtons()
                        : Container(),
                    domain.createForm.showEmojiKeyboard
                        ? EmojiListButtons()
                        : Container(),
                    domain.createForm.showDeadlines
                        ? FastChooseDeadline()
                        : Container(),
                    domain.createForm.showViewsList
                        ? ViewsListButtons()
                        : Container(),
                    const SizedBox(
                      height: 2,
                    ),

                    // const SizedBox(
                    //   height: 12,
                    // ),
                  ],
                ))
          ]));
    });
  }
}
