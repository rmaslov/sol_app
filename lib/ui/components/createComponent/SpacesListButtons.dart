import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/createComponent/SpaceLineButton.dart';

class SpacesListButtons extends StatelessWidget {
  const SpacesListButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return SizedBox(
        height: 40,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: domain.spacesStore.spaces
              .map((e) =>
                  SpaceLineButton(e.spaceResult.id!, e.spaceResult.fullTitle()))
              .toList(),
        ),
      );
    });
  }
}
