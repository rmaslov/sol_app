import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/components/createComponent/DateButton.dart';
import 'package:sol_app/ui/components/createComponent/DateTuneButton.dart';
import 'package:sol_app/ui/components/deadlineChange/DeadlineChange.dart';

class FastChooseDeadline extends StatelessWidget {
  const FastChooseDeadline({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return Row(
        children: [
          Spacer(),
          DateButton('Today', DateFormat('MMM dd').format(domain.createForm.fastDateToday()) , false,(){
            domain.createForm.setDeadline(domain.createForm.fastDateToday());
            domain.createForm.hideAll();
          }),
          Spacer(),
          DateButton('Tomorrow', DateFormat('MMM dd').format(domain.createForm.fastDateTomorrow()), false,(){
            domain.createForm.setDeadline(domain.createForm.fastDateTomorrow());
            domain.createForm.hideAll();
          }),
          Spacer(),
          DateButton('This week', DateFormat('MMM dd').format(domain.createForm.fastDateThisWeek()), false,(){
            domain.createForm.setDeadline(domain.createForm.fastDateThisWeek());
            domain.createForm.hideAll();
          }),
          Spacer(),
          DateButton('Next week', DateFormat('MMM dd').format(domain.createForm.fastDateNextWeek()), false,(){
            domain.createForm.setDeadline(domain.createForm.fastDateNextWeek());
            domain.createForm.hideAll();
          }),
          Spacer(),
          DateTuneButton(() {
            domain.createForm.hideAll();
            showCupertinoModalBottomSheet<void>(
              context: context,
              builder: (BuildContext context) {
                return DeadlineChange(
                    domain.createForm.request.deadline != null
                        ? DateTime.fromMillisecondsSinceEpoch(
                            domain.createForm.request.deadline!)
                        : DateTime.now(), (DateTime dt) {
                  domain.createForm.setDeadline(dt);
                }, () {
                  domain.createForm.setDeadline(null);

                  Navigator.pop(context);
                });
              },
            );
          }),
          Spacer()
        ],
      );
    });
  }
}
