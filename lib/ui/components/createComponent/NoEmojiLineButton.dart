import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class NoEmojiLineButton extends StatelessWidget {
  const NoEmojiLineButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return SizedBox(
          height: 42,
          child: TextButton(
              onPressed: () {
                domain.createForm.setEmoji('');
              },
              style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.zero)),
              child: Text(
                'No icon',
                style:  TextStyle(
                    fontWeight: FontWeight.w500,
                    color: SolColors.current.text.primary,
                    fontSize: 12),
              )));
    });
  }
}
