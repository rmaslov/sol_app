import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class SpaceLineButton extends StatelessWidget {
  const SpaceLineButton(this.spaceId, this.spaceTitle,{Key? key})
      : super(key: key);
  final String spaceId;
  final String spaceTitle;

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return SizedBox(
          height: 42,

          child: TextButton(
              onPressed: () {
                domain.createForm.setSpace(spaceId, spaceTitle);
                domain.createForm.hideAll();
              },
              style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.fromLTRB(8, 0, 8, 0)),

              ),
              
              child: Text(
                spaceTitle,

                style:  TextStyle(
                    fontWeight: FontWeight.w500,
                    color: SolColors.current.text.primary,
                    //backgroundColor: SolColors.current.button.background,

                    fontSize: 12),
              )));
    });
  }
}
