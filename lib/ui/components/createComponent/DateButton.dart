import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class DateButton extends StatelessWidget {
  const DateButton(this.title, this.subTitle, this.picked,this.onPressed, {Key? key})
      : super(key: key);
  final String title;
  final String subTitle;
  final bool picked;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
        onPressed: onPressed,
        style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.all(0.0)),
            side: MaterialStateProperty.all(BorderSide(
                width: 0.0,
                color: picked == false
                    ? SolColors.current.create.dateDontPickedBackground
                    : SolColors.current.create.datePickedBackground)),
            backgroundColor: MaterialStateProperty.all(picked == false
                ? SolColors.current.create.dateDontPickedBackground
                : SolColors.current.create.datePickedBackground)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.w500,
                  color: picked == false
                      ? SolColors.current.create.titleDontPicked
                      : SolColors.current.create.titlePicked),
            ),
            Text(subTitle,
                style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                    color: SolColors.current.create.subTitle))
          ],
        ));
  }
}
