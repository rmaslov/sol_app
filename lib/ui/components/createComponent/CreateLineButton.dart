import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class CreateLineButton extends StatelessWidget {
  const CreateLineButton(this.icon, this.active, this.onPressedIcon, {Key? key}) : super(key: key);
  final String icon;
  final VoidCallback onPressedIcon;
  final bool active;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 42,
        height: 42,
        child: TextButton(
            onPressed: onPressedIcon,
            style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.zero)),
            child: Container(
              width: 24,
              height: 24,
              padding: EdgeInsets.zero,
              alignment: Alignment.center,
              child: Image.asset(
                icon,
                width: 24,
                height: 24,
                  color: active ? Color.fromRGBO(255, 255, 255, 1) : Color.fromRGBO(255, 255, 255, 0.2),
                  colorBlendMode: BlendMode.modulate
              ),
            )));
  }
}
