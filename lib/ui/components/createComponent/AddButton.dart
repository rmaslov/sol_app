import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class AddButton extends StatelessWidget {
  const AddButton(this.available, this.onPressed, {Key? key}) : super(key: key);
  final bool available;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 38,
        height: 38,
        child: TextButton(
            onPressed: onPressed,
            style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.zero)),
            child: Container(
              width: 38,
              height: 38,
              padding: EdgeInsets.zero,
              alignment: Alignment.center,
              child: Image.asset(
                 'assets/ic_arrow_up.png',
                width: 24,
                height: 24,
                color: Colors.white.withOpacity(available ? 1 : 0.5),
                colorBlendMode: BlendMode.modulate,
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(color: SolColors.current.create.border),
                  color: SolColors.current.create.createBackground.withOpacity(available ? 1 : 0.5)),
            )));
  }
}
