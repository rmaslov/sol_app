import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class CloseBackground extends StatelessWidget {
  const CloseBackground(this.onPressed, {Key? key}) : super(key: key);
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 0,
      top: 0,
      right: 0,
      bottom: 0,
      child: Container(
        color: SolColors.current.create.backgroundClose,
        child: TextButton(onPressed: onPressed ,child: const Text(''),),
      ),
    );
  }
}
