import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class DateTuneButton extends StatelessWidget {
  const DateTuneButton(this.onPressed, {Key? key}) : super(key: key);
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
        onPressed: onPressed,
        style: ButtonStyle(
            side: MaterialStateProperty.all(BorderSide(
                width: 0.0,
                color: SolColors.current.create.dateDontPickedBackground)),
            backgroundColor: MaterialStateProperty.all(
                SolColors.current.create.dateDontPickedBackground)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/ic_tune.png',
              width: 24,
              height: 24,
            ),
            Text('Tune',
                style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                    color: SolColors.current.create.subTitle))
          ],
        ));
  }
}
