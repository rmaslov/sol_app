import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/infrastructure/api/view/response/ViewResult.dart';
import 'package:sol_app/ui/styles/SolColors.dart';

class ViewLineButton extends StatelessWidget {
  const ViewLineButton(this.viewResult, this.active, {Key? key})
      : super(key: key);
  final ViewResult viewResult;
  final bool active;

  @override
  Widget build(BuildContext context) {
    return Consumer<CoreDomain>(builder: (context, domain, child) {
      return Container(
          height: 42,
          padding: EdgeInsets.fromLTRB(4, 4, 4, 4),
          child: TextButton(
              onPressed: () {
                //domain.createForm.setSpace(viewId, title);
                domain.createForm.toggleView(viewResult);
                // domain.createForm.hideAll();
              },
              style: ButtonStyle(
                  padding: MaterialStateProperty.all(
                      EdgeInsets.fromLTRB(8, 0, 8, 0)),
                  backgroundColor: MaterialStateProperty.all(active
                      ? SolColors.current.create.buttonLineBackgroundActive
                      : SolColors.current.create.buttonLineBackground)),
              child: Text(
                viewResult.view!.fullTitle(),
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: SolColors.current.text.primary,
                    //backgroundColor: SolColors.current.button.background,

                    fontSize: 12),
              )));
    });
  }
}
