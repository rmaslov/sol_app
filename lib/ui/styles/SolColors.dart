import 'package:flutter/cupertino.dart';
import 'package:sol_app/domain/core/entity/meeting/MeetingType.dart';

class SolColors {
  static SolColors current = SolColors();

  Color gen = SolColorDay.black_68;
  Color logo = SolColorDay.red_218;

  SolColorBackground background = SolColorBackground();
  SolColorText text = SolColorText();
  SolColorField field = SolColorField();
  SolColorButton button = SolColorButton();
  SolColorSurface surface = SolColorSurface();
  SolColorIcon icon = SolColorIcon();
  SolColorStory story = SolColorStory();
  SolColorCreate create = SolColorCreate();
  SolColorSpaceItem spaceItem = SolColorSpaceItem();
  SolColorBottomPanel bottomPanel = SolColorBottomPanel();
  SolColorMeeting meeting = SolColorMeeting();
  SolColorHr hr = SolColorHr();
  SolColorCalendar calendar = SolColorCalendar();
}

class SolColorCalendar {
  Color bottomBackground = SolColorDay.white_246;
  Color bottomHr = SolColorDay.grey_198;
}


class SolColorMeeting {
  Color draft = Color.fromRGBO(117, 117, 117, 1);
  Color slot = Color.fromRGBO(46, 125, 50, 1);
  Color eventCalendar = Color.fromRGBO(25, 118, 210, 1);

  Color color(MeetingType meetingType) {
    if (meetingType == MeetingType.draft) {
      return draft;
    } else if (meetingType == MeetingType.slot) {
      return slot;
    } else {
      return eventCalendar;
    }
  }
}

class SolColorHr {
  Color hrLine = SolColorDay.black_150;
  Color hrLineLite = SolColorDay.grey_196;
}

class SolColorBottomPanel {
  Color background = SolColorDay.black_68;
  Color textColor = SolColorDay.white_246;
}

class SolColorSpaceItem {
  Color backgroundToggleClose = SolColorDay.white_255;
  Color backgroundToggleOpen = SolColorDay.white_250;
}

class SolColorCreate {
  Color backgroundClose = const Color.fromRGBO(0, 0, 0, 0.3);
  Color background = SolColorDay.white_255;
  Color createBackground = SolColorDay.black_68;
  Color border = SolColorDay.black_opacity_10;
  Color hr = SolColorDay.grey_198;
  Color subTitle = SolColorDay.grey_198;
  Color titlePicked = SolColorDay.white_255;
  Color titleDontPicked = SolColorDay.black_68;

  Color datePickedBackground = SolColorDay.black_68;
  Color dateDontPickedBackground = SolColorDay.white_255;

  Color buttonLineBackground = SolColorDay.white_255;
  Color buttonLineBackgroundActive = SolColorDay.black_opacity_10;
}

class SolColorBackground {
  Color primary = SolColorDay.white_255;
}

class SolColorStory {
  Color background = SolColorDay.white_246;
  Color space = SolColorDay.white_255;
  Color hasNewTask = SolColorDay.black_68;
  Color hasNotNewTask = SolColorDay.grey_198;
  Color color = SolColorDay.red_206;
  Color countHasNotText = SolColorDay.black_68;
  Color countHasNotBackground = SolColorDay.white_246;
  Color countHasText = SolColorDay.white_246;
  Color countHasBackground = SolColorDay.black_68;
  Color text = SolColorDay.black_68;
}

class SolColorText {
  Color primary = SolColorDay.black_68;
  Color placeholder = SolColorDay.grey_198;
  Color onColor = SolColorDay.white_246;
  Color error = SolColorDay.red_206;
}

class SolColorField {
  Color text = SolColorDay.black_68;
  Color textError = SolColorDay.red_206;
  Color placeholder = SolColorDay.grey_198;
  Color background = SolColorDay.white_246;
}

class SolColorButton {
  Color secondary = SolColorDay.white_240;
  Color disable = SolColorDay.white_246;
  Color primary = SolColorDay.black_68;

  Color text = SolColorDay.white_255;
  Color background = SolColorDay.black_68;

  Color textDisable = SolColorDay.grey_198;
  Color backgroundDisable = SolColorDay.white_246;

  Color emailText = SolColorDay.black_68;
  Color emailBackground = SolColorDay.white_240;

  Color appleBackground = SolColorDay.black_18;
}

class SolColorSurface {
  Color first = SolColorDay.white_246;
}

class SolColorIcon {
  Color onColor = SolColorDay.white_240;
  Color secondary = SolColorDay.white_225;
  Color tertiary = SolColorDay.tertiary_209;
  Color primary = SolColorDay.black_68;
}

class SolColorDay {
  static Color black_0 = const Color.fromRGBO(0, 0, 0, 1);
  static Color black_18 = const Color.fromRGBO(18, 18, 18, 0.8);
  static Color black_68 = const Color.fromRGBO(68, 68, 68, 1);
  static Color black_150 = const Color.fromRGBO(150, 150, 150, 1);
  static Color black_opacity_10 = const Color.fromRGBO(0, 0, 0, 0.1);
  static Color tertiary_209 = const Color.fromRGBO(209, 210, 221, 1);
  static Color grey_198 = const Color.fromRGBO(198, 198, 198, 1);
  static Color grey_196 = const Color.fromRGBO(196, 196, 196, 1);

  static Color white_255 = const Color.fromRGBO(255, 255, 255, 1.0);
  static Color white_250 = const Color.fromRGBO(250, 250, 250, 1.0);
  static Color white_246 = const Color.fromRGBO(246, 246, 246, 1);
  static Color white_240 = const Color.fromRGBO(240, 240, 240, 1);
  static Color white_225 = const Color.fromRGBO(225, 225, 225, 1);

  static Color red_218 = const Color.fromRGBO(218, 30, 30, 1);
  static Color red_206 = const Color.fromRGBO(206, 51, 79, 1);
}
