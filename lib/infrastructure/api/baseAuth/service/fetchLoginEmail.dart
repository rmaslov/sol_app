import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:sol_app/infrastructure/SolPreferences.dart';
import 'package:sol_app/infrastructure/api/baseAuth/request/LoginRequest.dart';
import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/response/LoginResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';


Future<LoginResponse> fetchLoginEmail(LoginRequest body) async {
  FetchResponse response =
      await fetchBasePost('/not-secure/api/v1/auth/login/email', body.toJson());

  if (response.code == 200) {
    // Album album = Album.fromJson(jsonDecode(response.body));
    LoginResponse obj = LoginResponse.fromJson(jsonDecode(response.body));
    if (obj.result != null && obj.result?.accessToken != null) {
      // SolPreferences.instance.setAccessToken(obj.result!.accessToken!);
      // SolPreferences.instance.setRefreshToken(obj.result!.refreshToken!);
    }
    return obj;
  }

  throw Exception(response.body);
}
