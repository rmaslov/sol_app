import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:sol_app/infrastructure/SolPreferences.dart';
import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';

import '../request/RefreshTokenRequest.dart';
import '../response/RefreshTokenResponse.dart';

final String HOST = dotenv.get("HOST");

Map<String, String> headerWithToken() {
  Map<String, String> header = {};
  // String? accessToken = SolPreferences.instance.accessToken();
  // if (accessToken != null) {
  //   header['X-Auth-Token'] = accessToken;
  // }
  return header;
}

Future<RefreshTokenResponse> fetchRefreshToken(RefreshTokenRequest body) async {
  FetchResponse response =
      await fetchBasePost('/not-secure/api/v1/auth/refresh', body.toJson());

  if (response.code == 200) {
    RefreshTokenResponse obj =
        RefreshTokenResponse.fromJson(jsonDecode(response.body));
    if (obj.result != null && obj.result?.accessToken != null) {
      // SolPreferences.instance.setAccessToken(obj.result!.accessToken!);
      // SolPreferences.instance.setRefreshToken(obj.result!.refreshToken!);
    }
    return obj;
  }

  throw Exception(response.body);
}

Future<FetchResponse> fetchBaseGet(String path) async {
  var headers = headerWithToken();
  final response = await http.get(Uri.parse(HOST + path), headers: headers);
  FetchResponse fetchResponse = FetchResponse('body', false, 0);

  if (response.statusCode == 200) {
    // Album album = Album.fromJson(jsonDecode(response.body));
    fetchResponse = FetchResponse(response.body, true, 200);
  } else if (response.statusCode == 401) {
    var refreshToken = "";//SolPreferences.instance.refreshToken();
    if (refreshToken != null) {
      try {
        RefreshTokenResponse response = await fetchRefreshToken(
            RefreshTokenRequest(refreshToken: refreshToken));
        final responseAfterRefreshToken =
            await http.get(Uri.parse(HOST + path), headers: headers);

        fetchResponse = FetchResponse(
            responseAfterRefreshToken.body,
            responseAfterRefreshToken.statusCode == 200,
            responseAfterRefreshToken.statusCode);
      } catch (e) {
        fetchResponse =
            FetchResponse(response.body, false, response.statusCode);
      }
    }
  } else {
    fetchResponse = FetchResponse(response.body, false, response.statusCode);
  }

  return Future.value(fetchResponse);
}

Future<FetchResponse> fetchBaseDelete(String path) async {
  var headers = headerWithToken();
  final response = await http.delete(Uri.parse(HOST + path), headers: headers);
  FetchResponse fetchResponse = FetchResponse('body', false, 0);

  if (response.statusCode == 200) {
    // Album album = Album.fromJson(jsonDecode(response.body));
    fetchResponse = FetchResponse(response.body, true, 200);
  } else if (response.statusCode == 401) {
    var refreshToken = ""; //SolPreferences.instance.refreshToken();
    if (refreshToken != null) {
      try {
        RefreshTokenResponse response = await fetchRefreshToken(
            RefreshTokenRequest(refreshToken: refreshToken));
        final responseAfterRefreshToken =
        await http.delete(Uri.parse(HOST + path), headers: headers);

        fetchResponse = FetchResponse(
            responseAfterRefreshToken.body,
            responseAfterRefreshToken.statusCode == 200,
            responseAfterRefreshToken.statusCode);
      } catch (e) {
        fetchResponse =
            FetchResponse(response.body, false, response.statusCode);
      }
    }
  } else {
    fetchResponse = FetchResponse(response.body, false, response.statusCode);
  }

  return Future.value(fetchResponse);
}


Future<FetchResponse> fetchBasePost(
    String path, Map<String, dynamic> body) async {
  var headers = headerWithToken();
  headers["Content-Type"] = "application/json";
  headers["Accepte"] = "application/json";

  final response = await http.post(Uri.parse(HOST + path),
      headers: headers,
      body: jsonEncode(body),
      encoding: Encoding.getByName("utf-8"));
  FetchResponse fetchResponse = FetchResponse('body', false, 0);

  if (response.statusCode == 200) {
    fetchResponse = FetchResponse(response.body, true, 200);
  } else if (response.statusCode == 401) {
    var refreshToken = ""; //SolPreferences.instance.refreshToken();
    if (refreshToken != null) {
      try {
        RefreshTokenResponse response = await fetchRefreshToken(
            RefreshTokenRequest(refreshToken: refreshToken));

        final responseAfterRefreshToken = await http.post(
            Uri.parse(HOST + path),
            headers: headers,
            body: jsonEncode(body),
            encoding: Encoding.getByName("utf-8"));

        fetchResponse = FetchResponse(
            responseAfterRefreshToken.body,
            responseAfterRefreshToken.statusCode == 200,
            responseAfterRefreshToken.statusCode);

      } catch (e) {
        fetchResponse =
            FetchResponse(response.body, false, response.statusCode);
      }
    }
  } else {
    fetchResponse = FetchResponse(response.body, false, response.statusCode);
  }

  return Future.value(fetchResponse);
}
