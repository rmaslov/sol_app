import 'dart:convert';

import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/response/MeResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';


Future<MeResponse?> fetchMe() async {
  FetchResponse response =
      await fetchBaseGet('/api/v1/user/me');

  if (response.code == 200) {
    MeResponse obj = MeResponse.fromJson(jsonDecode(response.body));
    return obj;
  }

  return null;
}
