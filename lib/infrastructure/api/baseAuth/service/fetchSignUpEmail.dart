import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:sol_app/infrastructure/SolPreferences.dart';
import 'package:sol_app/infrastructure/api/baseAuth/request/SignUpRequest.dart';
import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/response/SignUpResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';



Future<SignUpResponse> fetchSignUpEmail(SignUpRequest body) async {
  FetchResponse response = await fetchBasePost('/not-secure/api/v1/sign-up/email', body.toJson());

  if (response.code == 200) {
    // Album album = Album.fromJson(jsonDecode(response.body));
    SignUpResponse signUpResponse = SignUpResponse.fromJson(jsonDecode(response.body));
    if (signUpResponse.result != null && signUpResponse.result?.id != null){
      SolPreferences.instance.setSolId(signUpResponse.result!.id!);
    }

    return signUpResponse;
  }

  throw Exception(response.body);
}
