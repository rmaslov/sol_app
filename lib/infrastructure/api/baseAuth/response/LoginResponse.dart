class LoginResponse {
  Result? result;

  LoginResponse({this.result});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

class Result {
  String? accessToken;
  String? credentialId;
  String? refreshToken;

  Result({this.accessToken, this.credentialId, this.refreshToken});

  Result.fromJson(Map<String, dynamic> json) {
    accessToken = json['accessToken'];
    credentialId = json['credentialId'];
    refreshToken = json['refreshToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accessToken'] = this.accessToken;
    data['credentialId'] = this.credentialId;
    data['refreshToken'] = this.refreshToken;
    return data;
  }
}
