class FetchResponse{
  String body;
  bool isSuccess;
  int code;

  FetchResponse(this.body, this.isSuccess, this.code);
}