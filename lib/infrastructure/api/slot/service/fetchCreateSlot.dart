import 'dart:convert';

import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';
import 'package:sol_app/infrastructure/api/slot/request/SlotRequest.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotResponse.dart';
import 'package:sol_app/infrastructure/api/task/request/CreateTaskRequest.dart';
import 'package:sol_app/infrastructure/api/task/response/TaskResponse.dart';

Future<SlotResponse> fetchCreateSlot(SlotRequest request) async {
  FetchResponse response = await fetchBasePost('/api/v1/slot', request.toJson());

  if (response.code == 200) {
    SlotResponse obj = SlotResponse.fromJson(jsonDecode(response.body));
    return obj;
  }

  throw Exception(response.body);
}
