import 'dart:convert';

import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotResponse.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotsResponse.dart';

Future<SlotsResponse> fetchSlotsByDate(DateTime date) async {
  FetchResponse response = await fetchBaseGet(
      '/api/v1/slot?date=${date.millisecondsSinceEpoch}&timezone=${date.timeZoneOffset.inHours}');

  if (response.code == 200) {
    SlotsResponse obj = SlotsResponse.fromJson(jsonDecode(response.body));
    return obj;
  }

  return SlotsResponse(
      result: Result()
        ..count = 0
        ..items = []);
}

Future<SlotsResponse> fetchSlotsByTask(String taskId) async {
  FetchResponse response = await fetchBaseGet(
      '/api/v1/slot?taskId=${taskId}');

  if (response.code == 200) {
    SlotsResponse obj = SlotsResponse.fromJson(jsonDecode(response.body));
    return obj;
  }

  return SlotsResponse(
      result: Result()
        ..count = 0
        ..items = []);
}