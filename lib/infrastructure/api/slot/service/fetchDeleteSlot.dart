import 'dart:convert';

import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotResponse.dart';
import 'package:sol_app/infrastructure/api/slot/response/SlotsResponse.dart';

Future<void> fetchDeleteSlot(String slotId) async {
  FetchResponse response = await fetchBaseGet(
      '/api/v1/slot/${slotId}');

  if (response.code == 200) {
    return ;
  }

}
