import 'package:sol_app/infrastructure/api/slot/response/SlotResult.dart';

class SlotResponse {
  SlotResult? result;

  SlotResponse({this.result});

  SlotResponse.fromJson(Map<String, dynamic> json) {
    result =
    json['result'] != null ? new SlotResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

