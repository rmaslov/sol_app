class SlotResult {
  String? id;
  String? title;
  String? ownerId;
  String? createdFromTaskId;
  String? spaceId;
  List<String>? viewIds;
  int? startTime;
  int? endTime;
  int? slotsMilliseconds;
  List<String>? externalIds;
  int? timezone;
  int? createdAt;
  int? updatedAt;

  SlotResult(
      {this.id,
        this.title,
        this.ownerId,
        this.createdFromTaskId,
        this.spaceId,
        this.viewIds,
        this.startTime,
        this.endTime,
        this.slotsMilliseconds,
        this.externalIds,
        this.timezone,
        this.createdAt,
        this.updatedAt});

  SlotResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    ownerId = json['ownerId'];
    createdFromTaskId = json['createdFromTaskId'];
    spaceId = json['spaceId'];
    viewIds = json['viewIds'].cast<String>();
    startTime = json['startTime'];
    endTime = json['endTime'];
    slotsMilliseconds = json['slotsMilliseconds'];
    externalIds = json['externalIds'].cast<String>();
    timezone = json['timezone'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['ownerId'] = this.ownerId;
    data['createdFromTaskId'] = this.createdFromTaskId;
    data['spaceId'] = this.spaceId;
    data['viewIds'] = this.viewIds;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['slotsMilliseconds'] = this.slotsMilliseconds;
    data['externalIds'] = this.externalIds;
    data['timezone'] = this.timezone;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
