import 'package:sol_app/infrastructure/api/slot/response/SlotResult.dart';

class SlotsResponse {
  Result? result;

  SlotsResponse({this.result});

  SlotsResponse.fromJson(Map<String, dynamic> json) {
    result =
        json['result'] != null ? Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (result != null) {
      data['result'] = result!.toJson();
    }
    return data;
  }
}

class Result {
  int? count;
  List<SlotResult>? items;

  Result({this.count, this.items});

  Result.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['items'] != null) {
      items = <SlotResult>[];
      json['items'].forEach((v) {
        items!.add( SlotResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['count'] = count;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
