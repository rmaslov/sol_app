class SlotRequest {
  int? endTime;
  int? startTime;
  String? taskId;
  int? timezone;

  SlotRequest({this.endTime, this.startTime, this.taskId, this.timezone});

  SlotRequest.fromJson(Map<String, dynamic> json) {
    endTime = json['endTime'];
    startTime = json['startTime'];
    taskId = json['taskId'];
    timezone = json['timezone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['endTime'] = this.endTime;
    data['startTime'] = this.startTime;
    data['taskId'] = this.taskId;
    data['timezone'] = this.timezone;
    return data;
  }
}
