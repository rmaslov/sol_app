import 'package:sol_app/infrastructure/utils/StringUtils.dart';

class SpacesResponse {
  List<SpaceResult>? result;

  SpacesResponse({this.result});

  SpacesResponse.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = <SpaceResult>[];
      json['result'].forEach((v) {
        result!.add( SpaceResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  Map<String, dynamic>();
    if (result != null) {
      data['result'] = result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SpaceResult {
  String? id;
  String? title;
  IconResult? icon;
  int? sortNum;
  int? countTask;

  SpaceResult({id, title, icon, sortNum, countTask});

  String fullTitle() {
    String i = safe(icon?.data);
    String t = safe(title);
    if (i != '') return i + ' ' + t;
    return t;
  }

  SpaceResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    icon = json['icon'] != null ?  IconResult.fromJson(json['icon']) : null;
    sortNum = json['sortNum'];
    countTask = json['countTask'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['title'] = title;
    if (icon != null) {
      data['icon'] = icon!.toJson();
    }
    data['sortNum'] = sortNum;
    data['countTask'] = countTask;
    return data;
  }
}

class IconResult {
  String? data;
  String? type;

  IconResult({data, type});

  IconResult.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    type = json['type'];
  }

  Map<String, String?> toJson() {
    final Map<String, String?> data =  <String, String?>{};
    data['data'] = this.data;
    data['type'] = type;
    return data;
  }
}
