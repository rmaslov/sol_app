import 'dart:convert';


import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';

import '../response/SpacesResponse.dart';

Future<SpacesResponse> fetchSpaces() async {
  FetchResponse response =
  await fetchBaseGet('/api/v1/space');

  if (response.code == 200) {
    SpacesResponse obj = SpacesResponse.fromJson(jsonDecode(response.body));
    return obj;
  }

  return SpacesResponse(result: []);
}
