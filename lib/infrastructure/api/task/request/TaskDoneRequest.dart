import 'package:sol_app/infrastructure/api/space/response/SpacesResponse.dart';

class TaskDoneRequest {
  String id;

  TaskDoneRequest(this.id);
}
