import 'package:sol_app/infrastructure/api/space/response/SpacesResponse.dart';

class CreateTaskRequest {
  int? deadline;
  IconResult? icon;
  String? parentTaskId;
  List<String>? planningPoints = [];
  List<int>? reminders = [];
  String? repeatTaskConfId;
  String? spaceId;
  String? title = '';
  List<String>? viewIds = [];

  CreateTaskRequest(
      {this.deadline,
      this.icon,
      this.parentTaskId,
      this.planningPoints,
      this.repeatTaskConfId,
      this.spaceId,
      this.title,
      this.viewIds});

  String titleWithIcon(){
    String result = '';
    if(icon != null && icon?.data != null){
      result += icon!.data!;
    }
    if(title != null){
      result += ' ' + title!;
    }
    return result;
  }

  CreateTaskRequest.fromJson(Map<String, dynamic> json) {
    deadline = json['deadline'];
    icon = json['icon'] != null ? IconResult.fromJson(json['icon']) : null;
    parentTaskId = json['parentTaskId'];
    planningPoints = json['planningPoints'].cast<String>();
    reminders = json['reminders'].cast<String>();
    repeatTaskConfId = json['repeatTaskConfId'];
    spaceId = json['spaceId'];
    title = json['title'];
    viewIds = json['viewIds'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['deadline'] = this.deadline;
    if (this.icon != null) {
      data['icon'] = this.icon!.toJson();
    }
    data['parentTaskId'] = this.parentTaskId;
    data['planningPoints'] = this.planningPoints;
    data['reminders'] = this.reminders;
    data['repeatTaskConfId'] = this.repeatTaskConfId;
    data['spaceId'] = this.spaceId;
    data['title'] = this.title;
    data['viewIds'] = this.viewIds;
    return data;
  }
}
