import 'package:sol_app/infrastructure/api/task/response/TaskResult.dart';

class TasksResponse {
  List<TaskResult>? result;

  TasksResponse({result});

  TasksResponse.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = <TaskResult>[];
      json['result'].forEach((v) {
        result!.add(TaskResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (result != null) {
      data['result'] = result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


