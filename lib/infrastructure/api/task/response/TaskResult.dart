import 'package:sol_app/infrastructure/api/space/response/SpacesResponse.dart';
import 'package:sol_app/infrastructure/utils/StringUtils.dart';
enum TaskStatus{
  OPEN, DONE
}
class TaskResult {
  String? id;
  String? ownerId;
  String? parentTaskId;
  String? spaceId;
  String? title;
  IconResult? icon;
  List<String>? viewIds;
  List<String>? suggestForViewIds;
  bool? deadlineChangeFewTimes;
  List<String>? pics;
  List<String>? files;
  String? description;
  List<String>? externalIds;
  int? slotsMilliseconds;
  TaskStatus? status;
  List<TaskResult>? child;

  TaskResult(
      {id,
      ownerId,
      parentTaskId,
      spaceId,
      title,
      icon,
      viewIds,
      suggestForViewIds,
      deadlineChangeFewTimes,
      pics,
      files,
      description,
      externalIds,
      slotsMilliseconds,
      status,
      child});

  String iconAndTitle(){
    String iconString = safe(icon?.data);
    if(iconString != '') iconString += ' ';
    return iconString + safe(title);
  }

  TaskResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ownerId = json['ownerId'];
    parentTaskId = json['parentTaskId'];
    spaceId = json['spaceId'];
    title = json['title'];
    if(json['icon'] != null){
      icon = IconResult.fromJson(json['icon']);
    }
    viewIds = json['viewIds'].cast<String>();
    suggestForViewIds = json['suggestForViewIds'].cast<String>();
    deadlineChangeFewTimes = json['deadlineChangeFewTimes'];
    pics = json['pics'].cast<String>();
    files = json['files'].cast<String>();
    description = json['description'];
    externalIds = json['externalIds'].cast<String>();
    slotsMilliseconds = json['slotsMilliseconds'];
    status = json['status'] == 'DONE' ? TaskStatus.DONE : TaskStatus.OPEN;
    if (json['child'] != null) {
      child = <TaskResult>[];
      json['child'].forEach((v) {
        child!.add( TaskResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['ownerId'] = ownerId;
    data['parentTaskId'] = parentTaskId;
    data['spaceId'] = spaceId;
    data['title'] = title;
    if(icon != null) {
      data['icon'] = icon!.toJson();
    }
    data['viewIds'] = viewIds;
    data['suggestForViewIds'] = suggestForViewIds;
    data['deadlineChangeFewTimes'] = deadlineChangeFewTimes;
    data['pics'] = pics;
    data['files'] = files;
    data['description'] = description;
    data['externalIds'] = externalIds;
    data['slotsMilliseconds'] = slotsMilliseconds;
    data['status'] = status == TaskStatus.DONE ? 'DONE' : 'OPEN';
    if (child != null) {
      data['child'] = child!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
