import 'package:sol_app/infrastructure/api/task/response/TaskResult.dart';

class TaskResponse {
  TaskResult? result;

  TaskResponse({this.result});

  TaskResponse.fromJson(Map<String, dynamic> json) {
    result =
    json['result'] != null ? new TaskResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

