import 'dart:convert';

import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';
import 'package:sol_app/infrastructure/api/task/response/TasksResponse.dart';

Future<TasksResponse> fetchTasks() async {
  FetchResponse response = await fetchBaseGet('/api/v1/task');

  if (response.code == 200) {
    TasksResponse obj = TasksResponse.fromJson(jsonDecode(response.body));
    return obj;
  }

  return TasksResponse(result: []);
}
