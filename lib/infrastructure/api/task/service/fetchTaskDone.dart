import 'dart:convert';

import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';
import 'package:sol_app/infrastructure/api/task/request/CreateTaskRequest.dart';
import 'package:sol_app/infrastructure/api/task/request/TaskDoneRequest.dart';
import 'package:sol_app/infrastructure/api/task/response/TaskResponse.dart';

Future<TaskResponse> fetchTaskDone(TaskDoneRequest request) async {
  FetchResponse response = await fetchBasePost('/api/v1/task/'+request.id+'/done', <String, dynamic>{});

  if (response.code == 200) {
    TaskResponse obj = TaskResponse.fromJson(jsonDecode(response.body));
    return obj;
  }

  throw Exception(response.body);
}
