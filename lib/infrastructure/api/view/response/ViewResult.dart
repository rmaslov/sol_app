
import 'package:sol_app/infrastructure/api/view/response/View.dart';

class ViewResult {
  String? id;
  String? ownerId;
  String? createdFromTemplateId;
  bool? hasNewTaskToAdd;
  bool? hasTaskAdded;
  bool? canEdit;
  View? view;

  ViewResult(
      {this.id,
        this.ownerId,
        this.createdFromTemplateId,
        this.hasNewTaskToAdd,
        this.hasTaskAdded,
        this.canEdit,
        this.view});

  ViewResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ownerId = json['ownerId'];
    createdFromTemplateId = json['createdFromTemplateId'];
    hasNewTaskToAdd = json['hasNewTaskToAdd'];
    hasTaskAdded = json['hasTaskAdded'];
    canEdit = json['canEdit'];
    view = json['view'] != null ?  View.fromJson(json['view']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ownerId'] = this.ownerId;
    data['createdFromTemplateId'] = this.createdFromTemplateId;
    data['hasNewTaskToAdd'] = this.hasNewTaskToAdd;
    data['hasTaskAdded'] = this.hasTaskAdded;
    data['canEdit'] = this.canEdit;
    if (this.view != null) {
      data['view'] = this.view!.toJson();
    }
    return data;
  }
}



