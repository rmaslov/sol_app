
import 'package:sol_app/infrastructure/api/view/response/ParamType.dart';
import 'package:sol_app/infrastructure/utils/EnumUtils.dart';

class Params {
  String? id;
  ParamType? type;
  String? valueString;
  String? valueDate;
  bool? valueBool;

  Params(
      {this.id, this.type, this.valueString, this.valueDate, this.valueBool});

  Params.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = enumFromString(ParamType.values, json['type']);
    valueString = json['valueString'];
    valueDate = json['valueDate'];
    valueBool = json['valueBool'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['valueString'] = enumToString(this.valueString);
    data['valueDate'] = this.valueDate;
    data['valueBool'] = this.valueBool;
    return data;
  }
}
