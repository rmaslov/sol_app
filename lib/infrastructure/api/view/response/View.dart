import 'package:sol_app/infrastructure/api/space/response/SpacesResponse.dart';
import 'package:sol_app/infrastructure/api/view/response/AddedType.dart';
import 'package:sol_app/infrastructure/api/view/response/DisplayMode.dart';
import 'package:sol_app/infrastructure/api/view/response/Params.dart';
import 'package:sol_app/infrastructure/api/view/response/SortType.dart';
import 'package:sol_app/infrastructure/api/view/response/ViewType.dart';
import 'package:sol_app/infrastructure/utils/EnumUtils.dart';
import 'package:sol_app/infrastructure/utils/StringUtils.dart';

class View {
  IconResult? icon;
  String? title;
  String? description;
  AddedType? addedType;
  DisplayMode? displayMode;
  SortType? sortType;
  ViewType? viewType;
  List<Params>? params;

  String fullTitle() {
    String i = safe(icon?.data);
    String t = safe(title);
    if (i != '') return i + ' ' + t;
    return t;
  }

  View(
      {this.icon,
      this.title,
      this.description,
      this.addedType,
      this.displayMode,
      this.sortType,
      this.viewType,
      this.params});

  View.fromJson(Map<String, dynamic> json) {
    icon = json['icon'] != null ? new IconResult.fromJson(json['icon']) : null;
    title = json['title'];
    description = json['description'];
    addedType = enumFromString(AddedType.values, json['addedType']);
    displayMode = enumFromString(DisplayMode.values, json['displayMode']);
    sortType = enumFromString(SortType.values, json['sortType']);
    viewType = enumFromString(ViewType.values, json['viewType']);
    if (json['params'] != null) {
      params = <Params>[];
      json['params'].forEach((v) {
        params!.add(Params.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.icon != null) {
      data['icon'] = this.icon!.toJson();
    }
    data['title'] = this.title;
    data['description'] = this.description;
    data['addedType'] = enumToString(this.addedType);
    data['displayMode'] = enumToString(this.displayMode);
    data['sortType'] = enumToString(this.sortType);
    data['viewType'] = enumToString(this.viewType);
    if (this.params != null) {
      data['params'] = this.params!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
