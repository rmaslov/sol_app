import 'package:sol_app/infrastructure/api/view/response/ViewResult.dart';

class ViewsResponse {
  List<ViewResult>? result;

  ViewsResponse({this.result});

  ViewsResponse.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = <ViewResult>[];
      json['result'].forEach((v) {
        result!.add(ViewResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
