import 'dart:convert';

import 'package:sol_app/infrastructure/api/baseAuth/response/FetchResponse.dart';
import 'package:sol_app/infrastructure/api/baseAuth/service/fetchBase.dart';
import 'package:sol_app/infrastructure/api/view/response/ViewsResponse.dart';

Future<ViewsResponse> fetchViews() async {
  FetchResponse response = await fetchBaseGet('/api/v1/view-user/all');

  if (response.code == 200) {
    try {
      ViewsResponse obj = ViewsResponse.fromJson(jsonDecode(response.body));
      return obj;
    }catch(e){
      print(e);
    }
  }

  return ViewsResponse(result: []);
}
