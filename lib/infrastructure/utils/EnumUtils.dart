T enumFromString<T>(Iterable<T> values, String value) {
  return values.firstWhere((type) => type.toString().split(".").last == value,
      orElse: null);
}

String enumToString<T>(T values) {
  return values.toString().split(".").last;
}
