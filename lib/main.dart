// Copyright 2020 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:sol_app/sol_app_router.dart';
import 'package:sol_app/domain/core/domain/CoreDomain.dart';
import 'package:sol_app/domain/user/domain/UserDomain.dart';
import 'package:sol_app/ui/pages/general/GeneralPage.dart';
import 'package:sol_app/ui/pages/login/LoadingPage.dart';
import 'package:sol_app/ui/pages/login/LoginByEmailPage.dart';
import 'package:sol_app/ui/pages/login/LoginPage.dart';
import 'package:sol_app/ui/pages/ui_kit_page_example/UIKitPage.dart';
// import 'package:ftl_marketing_flutter_lib/FCMManager.dart';


void _changeData(notificationData) {}

// final firebaseMessaging = FCMManager();

Future main() async {
  await dotenv.load(fileName: 'env/.env');
  UserDomain userDomain = UserDomain();
  CoreDomain coreDomain = CoreDomain(userDomain);
  coreDomain.setup();

  await init();

  if(defaultTargetPlatform == TargetPlatform.iOS || defaultTargetPlatform == TargetPlatform.android){
    // firebaseMessaging.setNotifications();
    // firebaseMessaging.streamCtlr.stream.listen(_changeData);
    // firebaseMessaging.bodyCtlr.stream.listen(_changeData);
    // firebaseMessaging.titleCtlr.stream.listen(_changeData);
  }

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => userDomain),
      ChangeNotifierProvider(create: (context) => coreDomain),
    ],

    //TODO - вот это все поменять и заменить на GoRouter
    child: CupertinoApp(
      title: 'Sol.app',
      initialRoute: Routes.root,
      routes: {
        Routes.loginByEmail: (context) => const LoginByEmail(),
        Routes.login: (context) => const LoginPage(),
        Routes.general: (context) => const GeneralPage(),
        //Routes.calendar: (context) => const CalendarPage(CalendarOpenFrom.fromDefault),
        Routes.root: (context) => UIKitPage(),
      },
    ),
  ));
}

Future init() async {
  if(defaultTargetPlatform == TargetPlatform.iOS || defaultTargetPlatform == TargetPlatform.android){
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
  }
}
